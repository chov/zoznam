import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ContactComponent} from "./contact.component";
import {RouterLink, RouterModule} from "@angular/router";
import {AddressModule} from "../address/address.module";

@NgModule({
  declarations: [ContactComponent],
  imports: [CommonModule, RouterModule,
    RouterLink, AddressModule],
  exports: [ContactComponent]
})
export class ContactModule {
}
