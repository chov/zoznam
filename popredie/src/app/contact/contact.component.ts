import {Component, Input} from '@angular/core';
import {ContactInterface} from "../types/contact.interface";
import {ContactService} from "../services/contact.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent {
  constructor(private contactService: ContactService) {
  }

  //Component takes input from HomeComponent ngFor(contacts list)
  @Input() contact!: ContactInterface;

  deleteContactFunction(contactId: number) {
    this.contactService.deleteContact(contactId).subscribe(response =>
      console.log(response));
  }

}
