import {AdminContactInterface} from "./admin.contact.interface";

export interface AdminPageResponse {

  contacts: AdminContactInterface[],

  numOfFilteredContacts: number

}
