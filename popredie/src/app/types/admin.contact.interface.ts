import {AddressInterface} from "./address.interface";

export interface AdminContactInterface {

  id: number;

  name: string;

  user: string;

  surname: string;

  age: number;

  birthdate: Date;

  phoneNum: string | null;

  address: AddressInterface;

}
