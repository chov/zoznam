import {AddressInterface} from "./address.interface";

/** Type interface for Contact,
 * also have attached address to follow schema in backend/db*/
export interface ContactInterface {

  id: number;

  name: string;

  surname: string;

  age: number;

  birthdate: Date;

  phoneNum: string | null;

  image: string | null;

  hobbies: string[];

  address: AddressInterface;

}
