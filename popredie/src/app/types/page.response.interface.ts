import {ContactInterface} from "./contact.interface";

export interface PageResponseInterface {

  contacts: ContactInterface[],

  numOfFilteredContacts: number

}
