import {TokenInterface} from "./token.interface";

export interface LoginResponseInterface {

  accessToken: TokenInterface,

  admin: boolean

}
