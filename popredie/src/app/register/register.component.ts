import {Component, inject} from '@angular/core';
import {RegisterInterface} from "../types/register.interface";
import {Router} from "@angular/router";
import {AddressInterface} from "../types/address.interface";
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {ContactInterface} from "../types/contact.interface";
import {ContactService} from "../services/contact.service";
import {MatChipEditedEvent, MatChipInputEvent} from '@angular/material/chips';
import {LiveAnnouncer} from '@angular/cdk/a11y';
import {COMMA, ENTER} from '@angular/cdk/keycodes';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  constructor(private contactService: ContactService,
              private fb: FormBuilder,
              private router: Router) {
  }

  reader!: FileReader;
  file!: File;

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  hobbies: string[] = [];
  announcer = inject(LiveAnnouncer);


  ngOnInit() {
    var reader = new FileReader();
    reader.addEventListener('loadend', function () {
      reader.onload = () => {
        console.log('data');
        console.log(reader.result);
      };

    });

  }

  contactForm: FormGroup = this.fb.group({
    nameField: ['', Validators.required],
    surnameField: ['', Validators.required],
    phNumField: ['', Validators.pattern('^(\\+\\d{3}|0)\\d{9}$')],
    birthdateField: [undefined, [this.pastDateValidator(), Validators.required]],
    imageField: [undefined, this.pngFileValidator()]
  });
  addressForm: FormGroup = this.fb.group({
    streetField: [''],
    cityField: [''],
    countryField: [''],
    houseNumField: [null]
  })
  parentForm: FormGroup = this.fb.group({
    contactForm: this.contactForm,
    addressForm: this.addressForm
  })

  async submitForm() {
    if (this.file === undefined) {
      console.log('for: ' + this.file);
    }
    if (this.contactForm.valid) {
      try {
        const contact: RegisterInterface = await this.convertToRegisterInterface();
        contact.address = this.convertToAddressInterface();
        console.log(contact);
        this.contactService.addContactAddress(contact).subscribe(resp => {
          if (this.instanceOfContactInterface(resp)) {
            this.contactService.addToRegisterStream(resp);
          }
        });
      } catch (error) {
        console.error(error);
      }
      this.router.navigate(['/']);
    } else {
      console.log('Form is invalid');
    }
  }

  instanceOfContactInterface(object: any): object is ContactInterface {
    return object;
  }

  private pastDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const selectedDate = new Date(control.value);
      const currentDate = new Date();

      if (selectedDate < currentDate) {
        return null;
      } else {
        return {
          'pastDate': true
        };
      }
    };
  }

  convertToAddressInterface(): AddressInterface {
    return {
      street: this.addressForm.get('streetField')?.value || null,
      houseNum: this.addressForm.get('houseNumField')?.value || null,
      city: this.addressForm.get('cityField')?.value || null,
      country: this.addressForm.get('countryField')?.value || null
    };
  }

  async convertToRegisterInterface(): Promise<RegisterInterface> {
    try {
      const base64Data = await this.getBase64(this.file);

      return {
        name: this.contactForm.get('nameField')?.value || '',
        surname: this.contactForm.get('surnameField')?.value || '',
        birthdate: this.contactForm.get('birthdateField')?.value || new Date(),
        phoneNum: this.contactForm.get('phNumField')?.value || null,
        image: base64Data,
        hobbies: this.hobbies,
        address: null
      };
    } catch (error) {
      console.error(error);

      return {
        name: this.contactForm.get('nameField')?.value || '',
        surname: this.contactForm.get('surnameField')?.value || '',
        birthdate: this.contactForm.get('birthdateField')?.value || new Date(),
        phoneNum: this.contactForm.get('phNumField')?.value || null,
        image: null,
        hobbies: this.hobbies,
        address: null
      };
    }
  }

  pngFileValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const file: File = control.value;
      if (control.value === null || control.value === undefined) {
        return null;
      }
      const allowedExtensions = ['png'];
      const fileExtension = file.toString().split('.').pop()?.toLowerCase();

      if (fileExtension && allowedExtensions.indexOf(fileExtension) === -1) {
        return {pngFile: true};
      }
      return null;
    };
  }

  onFilechange(event: any) {
    this.file = event.target.files[0]
  }

  getBase64(file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader: FileReader = new FileReader();

      reader.onload = function () {
        if (typeof reader.result === 'string') {
          resolve(reader.result);
        } else {
          reject(new Error('Invalid file type or data.'));
        }
      };

      reader.onerror = function (error) {
        reject(error);
      };

      reader.readAsDataURL(file);
    });
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.hobbies.push(value);
    }
    event.chipInput!.clear();
  }

  remove(hobby: string): void {
    const index = this.hobbies.indexOf(hobby);
    if (index >= 0) {
      this.hobbies.splice(index, 1);
      this.announcer.announce(`Removed ${hobby}`);
    }
  }

  edit(hobby: string, event: MatChipEditedEvent) {
    const value = event.value.trim();
    if (!value) {
      this.remove(hobby);
      return;
    }
    const index = this.hobbies.indexOf(hobby);
    if (index >= 0) {
      this.hobbies[index] = value;
    }
  }
}
