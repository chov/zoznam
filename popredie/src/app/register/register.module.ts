import {NgModule} from "@angular/core";
import {RegisterComponent} from "./register.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';

@NgModule({
  declarations: [RegisterComponent],
  imports: [CommonModule, FormsModule,
    ReactiveFormsModule, MatInputModule,
    MatFormFieldModule, MatIconModule, MatChipsModule],
  providers: [],
  exports: [RegisterComponent]
})
export class RegisterModule {
}
