import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ContactService} from "../services/contact.service";
import {RouterLink, RouterModule} from "@angular/router";
import {ContactModule} from "../contact/contact.module";
import {UpdateModule} from "../update/update.module";
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatSortModule} from '@angular/material/sort';
import {AdminComponent} from "./admin.component";
import {FilterAdminModule} from "../filter.admin/filter.admin.module";
import {FilterModule} from "../filter/filter.module";
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [AdminComponent],
  imports: [CommonModule, MatMenuModule, MatButtonModule,
    ContactModule, RouterLink, MatSortModule,
    RouterModule, UpdateModule, MatTableModule,
    MatPaginatorModule, FilterAdminModule, FilterModule,
    MatExpansionModule, MatIconModule],
  providers: [ContactService],
  exports: [AdminComponent]
})
export class AdminModule {
}
