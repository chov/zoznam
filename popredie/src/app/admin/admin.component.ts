import {ChangeDetectorRef, Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {ContactService} from "../services/contact.service";
import {ContactInterface} from "../types/contact.interface";
import {MatSort, Sort} from '@angular/material/sort';
import {PagingParams} from "../params/paging.params";
import {SortingParams} from "../params/sorting.params";
import {AdminContactInterface} from "../types/admin.contact.interface";
import {FilterParamsAdmin} from "../params/filter.params.admin";
import {Router} from '@angular/router';
import {UserService} from "../services/user.service";
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @ViewChild(MatSort) sorter!: MatSort;

  @ViewChild(MatTable) table!: MatTable<any>;

  filterComponentData!: FilterParamsAdmin;

  @Output() deleteContact: EventEmitter<ContactInterface> = new EventEmitter<ContactInterface>();

  isCollapsed: boolean = false;

  displayedColumns: string[] = ['user', 'name', 'surname', 'birthdate', 'phoneNum'];

  columnsToDisplayWithExpand = [...this.displayedColumns, 'options', 'expand'];

  expandedElement!: AdminContactInterface | null;


  constructor(private contactService: ContactService,
              private changeDetectorRef: ChangeDetectorRef,
              private router: Router,
              private loginService: UserService) {
  }

  contacts: AdminContactInterface[] = [];
  dataSource!: MatTableDataSource<AdminContactInterface>;

  ngOnInit(): void {
    const pagingParams: PagingParams = {
      offset: 0,
      limit: 5
    };

    this.contactService.getContactsAdmin(pagingParams, null, null)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<AdminContactInterface>(this.contacts);
      });

    this.contactService.adminFilterData$.subscribe(resp => {
      if (resp != null) {
        this.filterComponentData = resp;
        this.filterChange(resp);
      }
    })
  }

  filterChange(filterParam: FilterParamsAdmin) {
    const sortingParams: SortingParams = {
      active: this.sorter.active,
      direction: this.sorter.direction
    };
    const pagingParams: PagingParams = {
      offset: this.paginator.pageIndex,
      limit: this.paginator.pageSize
    };
    this.contactService.getContactsAdmin(pagingParams, sortingParams, filterParam)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<AdminContactInterface>(this.contacts);
      });
  }

  sortChange(sortState: Sort) {
    const sortingParams: SortingParams = {
      active: sortState.active,
      direction: sortState.direction
    };
    const pagingParams: PagingParams = {
      offset: this.paginator.pageIndex,
      limit: this.paginator.pageSize
    };
    this.contactService.getContactsAdmin(pagingParams, sortingParams, this.filterComponentData)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<AdminContactInterface>(this.contacts);
      });
  }

  public pageChange() {
    const sortingParams: SortingParams = {
      active: this.sorter.active,
      direction: this.sorter.direction
    };
    const pagingParams: PagingParams = {
      offset: this.paginator.pageIndex,
      limit: this.paginator.pageSize
    };
    this.contactService.getContactsAdmin(pagingParams, sortingParams, this.filterComponentData)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<AdminContactInterface>(this.contacts);
      });
  }

  deleteContactFunction(contactId: number) {
    this.contactService.deleteContact(contactId).subscribe(response =>
      console.log(response));
    const index = this.contacts.findIndex(contact => contact.id === contactId);
    if (index > -1) {
      this.contacts.splice(index, 1);
      this.changeDetectorRef.detectChanges();
      this.table.renderRows();
    }
  }

  deleteUser(user: string) {
    this.loginService.deleteUserAdmin(user).subscribe(() => {
      location.reload();
    })
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  onCollapsedChange(collapsed: boolean) {
    this.isCollapsed = collapsed;
  }

}
