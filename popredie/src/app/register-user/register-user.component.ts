import {Component} from '@angular/core';
import {UserService} from "../services/user.service";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserInterface} from "../types/user.interface";
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent {

  registerForm: FormGroup;

  hidePassword = true;

  hidePasswordMatch = true;

  constructor(private loginService: UserService,
              private fb: FormBuilder,
              private router: Router) {
    this.registerForm = this.fb.group({
      usernameField: ['', Validators.required],
      passwordField: ['', Validators.required],
      passwordMatchField: ['', Validators.required]
    });
    this.registerForm.addValidators(
      this.confirmPasswordValidator(
        this.registerForm.get('passwordField'),
        this.registerForm.get('passwordMatchField')
      )
    )
  }

  errorMessage!: true;

  submitForm() {
    if (this.registerForm.valid) {
      const user: UserInterface = this.convertToUserInterface();
      this.loginService.registerUser(user).subscribe(resp => {
        localStorage.setItem('accessToken', resp.accessToken.access_token);
        this.router.navigate(['']);
      }, error => {
        if (error.status == 409) {
          this.errorMessage = true;
        }
      });
    }
  }

  confirmPasswordValidator(password: AbstractControl | null, passwordMatch: AbstractControl | null) {
    return () => {
      if (password?.dirty === false || passwordMatch?.dirty === false) {
        return null;
      }
      if (password?.value !== passwordMatch?.value) {
        return {passwordsDontMatch: true};
      }
      return null;
    }
  }

  convertToUserInterface(): UserInterface {
    return {
      username: this.registerForm.get('usernameField')?.value || null,
      password: this.registerForm.get('passwordField')?.value || null
    }
  }

}
