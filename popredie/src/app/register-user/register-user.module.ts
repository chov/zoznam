import {NgModule} from "@angular/core";
import {RegisterUserComponent} from "./register-user.component";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [RegisterUserComponent],
  imports: [CommonModule, FormsModule,
    ReactiveFormsModule, MatInputModule,
    MatFormFieldModule, MatIconModule, MatButtonModule],
  providers: [],
  exports: [RegisterUserComponent]
})
export class RegisterUserModule {
}
