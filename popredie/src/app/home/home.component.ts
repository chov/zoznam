import {ChangeDetectorRef, Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {ContactService} from "../services/contact.service";
import {ContactInterface} from "../types/contact.interface";
import {MatSort, Sort} from '@angular/material/sort';
import {PagingParams} from "../params/paging.params";
import {SortingParams} from "../params/sorting.params";
import {FilterParams} from "../params/filter.params";
import {UserService} from "../services/user.service";
import {Router} from '@angular/router';
import {FilterComponent} from "../filter/filter.component";
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sorter!: MatSort;
  @ViewChild(MatTable) table!: MatTable<any>;
  @ViewChild('filter') filterComponent!: FilterComponent;
  filterComponentData!: FilterParams;
  @Output() deleteContact: EventEmitter<ContactInterface> = new EventEmitter<ContactInterface>();
  isCollapsed: boolean = false;

  expandedElement!: ContactInterface | null;
  displayedColumns: string[] = ['name', 'surname', 'birthdate', 'phoneNum'];
  columnsToDisplayWithExpand = [...this.displayedColumns, 'options', 'expand'];

  constructor(private contactService: ContactService,
              private changeDetectorRef: ChangeDetectorRef,
              private loginService: UserService,
              private router: Router) {
  }

  ngOnInit(): void {
    const pagingParams: PagingParams = {
      offset: 0,
      limit: 5
    };

    this.contactService.getContacts(pagingParams, null, null)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<ContactInterface>(this.contacts);
      });

    this.contactService.registerData$.subscribe(resp => {
      this.registerContact(resp);
    });

    this.contactService.updateData$.subscribe(resp => {
      this.updateContact(resp);
    });

    this.contactService.filterData$.subscribe(resp => {
      if (resp != null) {
        this.filterComponentData = resp;
        this.filterChange(resp);
      }
    })
  }

  filterChange(filterParam: FilterParams) {
    const sortingParams: SortingParams = {
      active: this.sorter.active,
      direction: this.sorter.direction
    };
    const pagingParams: PagingParams = {
      offset: this.paginator.pageIndex,
      limit: this.paginator.pageSize
    };
    this.contactService.getContacts(pagingParams, sortingParams, filterParam)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<ContactInterface>(this.contacts);
      });
  }

  sortChange(sortState: Sort) {
    const sortingParams: SortingParams = {
      active: sortState.active,
      direction: sortState.direction
    };
    const pagingParams: PagingParams = {
      offset: this.paginator.pageIndex,
      limit: this.paginator.pageSize
    };
    this.contactService.getContacts(pagingParams, sortingParams, this.filterComponentData)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<ContactInterface>(this.contacts);
      });
  }

  public pageChange() {
    const sortingParams: SortingParams = {
      active: this.sorter.active,
      direction: this.sorter.direction
    };
    const pagingParams: PagingParams = {
      offset: this.paginator.pageIndex,
      limit: this.paginator.pageSize
    };
    this.contactService.getContacts(pagingParams, sortingParams, this.filterComponentData)
      .subscribe(data => {
        this.paginator.length = data.numOfFilteredContacts;
        this.contacts = data.contacts;
        this.dataSource = new MatTableDataSource<ContactInterface>(this.contacts);
      });
  }


  deleteContactFunction(contactId: number) {
    this.contactService.deleteContact(contactId).subscribe(response =>
      console.log(response));
    const index = this.contacts.findIndex(contact => contact.id === contactId);
    if (index > -1) {
      this.contacts.splice(index, 1);
      this.changeDetectorRef.detectChanges();
      this.table.renderRows();
    }
  }

  onCollapsedChange(collapsed: boolean) {
    this.isCollapsed = collapsed;
  }

  contacts: ContactInterface[] = [];
  dataSource!: MatTableDataSource<ContactInterface>;

  registerContact(registeredContact: ContactInterface) {
    this.contacts.push(registeredContact);
    this.dataSource = new MatTableDataSource<ContactInterface>(this.contacts);
  }

  updateContact(updatedContact: ContactInterface) {
    const index = this.contacts.findIndex(contact => contact.id === updatedContact.id);
    if (index !== undefined && index >= 0 && index < this.contacts.length) {
      this.contacts[index] = updatedContact;
    }
    this.dataSource = new MatTableDataSource<ContactInterface>(this.contacts);
  }

  deleteUser() {
    this.loginService.deleteUser().subscribe(() => {
      this.router.navigate(['/login']);
    });
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
