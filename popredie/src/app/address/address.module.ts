import {NgModule} from "@angular/core";
import {AddressComponent} from "./address.component";
import {CommonModule} from "@angular/common";
import {RouterLink, RouterModule} from "@angular/router";


@NgModule({
  declarations: [AddressComponent],
  imports: [CommonModule, RouterModule, RouterLink],
  providers: [],
  exports: [AddressComponent]
})
export class AddressModule {
}
