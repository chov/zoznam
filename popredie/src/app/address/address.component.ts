import {Component, Input} from '@angular/core';
import {AddressInterface} from "../types/address.interface";

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})

export class AddressComponent {

  @Input() address!: AddressInterface | null;

  @Input() contactId!: number;

}
