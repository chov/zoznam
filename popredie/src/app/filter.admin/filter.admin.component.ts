import {Component, EventEmitter, inject, Output} from '@angular/core';
import {ContactService} from "../services/contact.service";
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {FilterParamsAdmin} from "../params/filter.params.admin";
import {LiveAnnouncer} from '@angular/cdk/a11y';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipEditedEvent, MatChipInputEvent} from '@angular/material/chips';


@Component({
  selector: 'app-filter-admin',
  templateUrl: './filter.admin.component.html',
  styleUrls: ['./filter.admin.component.css']
})
export class FilterAdminComponent {
  constructor(private fb: FormBuilder,
              private contactService: ContactService) {
  }

  isCollapsed: boolean = false;
  @Output() collapsedChange = new EventEmitter<boolean>();

  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
    this.collapsedChange.emit(this.isCollapsed);
  }

  hideRequiredControl = new FormControl(false);
  options = this.fb.group({
    hideRequired: this.hideRequiredControl,
  });

  filterForm: FormGroup = this.fb.group({
    userField: [null],
    nameField: [null],
    surnameField: [null],
    nameFieldExact: [false],
    userFieldExact: [false],
    surnameFieldExact: [false],
    birthdateField: [null],
    birthdateFieldExact: ['exact'],
    phoneNumField: [null],
    phoneNumFieldExact: [false],
    streetField: [null],
    streetFieldExact: [false],
    cityField: [null],
    cityFieldExact: [false],
    countryField: [null],
    countryFieldExact: [false]
  })

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  hobbies: string[] = [];
  announcer = inject(LiveAnnouncer);

  submitForm() {
    this.contactService.addToAdminFilterStream(this.convertToFilterParamInterface());
  }

  convertToFilterParamInterface(): FilterParamsAdmin {
    return {
      user: this.filterForm.get('userField')?.value || null,
      userExact: this.filterForm.get('userFieldExact')?.value,
      name: this.filterForm.get('nameField')?.value || null,
      nameExact: this.filterForm.get('nameFieldExact')?.value,
      surname: this.filterForm.get('surnameField')?.value || null,
      surnameExact: this.filterForm.get('surnameFieldExact')?.value,
      birthdate: this.formatDate(this.filterForm.get('birthdateField')?.value) || null,
      birthdateExact: this.filterForm.get('birthdateFieldExact')?.value || null,
      phoneNum: this.filterForm.get('phoneNumField')?.value || null,
      phoneNumExact: this.filterForm.get('phoneNumFieldExact')?.value,
      street: this.filterForm.get('streetField')?.value || null,
      streetExact: this.filterForm.get('streetFieldExact')?.value,
      city: this.filterForm.get('cityField')?.value || null,
      cityExact: this.filterForm.get('cityFieldExact')?.value,
      country: this.filterForm.get('countryField')?.value || null,
      countryExact: this.filterForm.get('countryFieldExact')?.value,
      hobbies: this.hobbies
    }
  }

  formatDate(date: Date | null): string | null {
    if (date === null) {
      return null;
    }
    const newDate = new Date(date);
    if (isNaN(newDate.getDate())) {
      return null;
    }
    const year = newDate.getFullYear();
    const month = ('0' + (newDate.getMonth() + 1)).slice(-2);
    const day = ('0' + newDate.getDate()).slice(-2);

    return `${year}-${month}-${day}`;
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.hobbies.push(value);
    }
    event.chipInput!.clear();
  }

  remove(hobby: string): void {
    const index = this.hobbies.indexOf(hobby);
    if (index >= 0) {
      this.hobbies.splice(index, 1);
      this.announcer.announce(`Removed ${hobby}`);
    }
  }

  edit(hobby: string, event: MatChipEditedEvent) {
    const value = event.value.trim();
    if (!value) {
      this.remove(hobby);
      return;
    }
    const index = this.hobbies.indexOf(hobby);
    if (index >= 0) {
      this.hobbies[index] = value;
    }
  }

}
