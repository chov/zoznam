import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {FilterAdminComponent} from "./filter.admin.component";
import {MatChipsModule} from '@angular/material/chips';


@NgModule({
  declarations: [FilterAdminComponent],
  imports: [CommonModule, FormsModule,
    ReactiveFormsModule, MatInputModule,
    MatFormFieldModule, MatCheckboxModule,
    MatIconModule, MatDatepickerModule,
    MatNativeDateModule, MatSelectModule, MatChipsModule],
  providers: [],
  exports: [FilterAdminComponent]
})
export class FilterAdminModule {

}
