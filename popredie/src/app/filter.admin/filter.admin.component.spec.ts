import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterAdminComponent } from './filter.admin.component';

describe('FilterAdminComponent', () => {
  let component: FilterAdminComponent;
  let fixture: ComponentFixture<FilterAdminComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FilterAdminComponent]
    });
    fixture = TestBed.createComponent(FilterAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
