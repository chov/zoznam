import {inject} from '@angular/core'
import {UserService} from "./services/user.service";

export const authGuard = () => {
  const loginService = inject(UserService);

  return new Promise<boolean>(resolve => {
    loginService.isUserAdmin().subscribe(resp => {
      resolve(resp);
    });
  })

}
