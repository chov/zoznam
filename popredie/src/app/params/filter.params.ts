export interface FilterParams {

  name: string | null,

  nameExact: boolean,

  surname: string | null,

  surnameExact: boolean,

  birthdate: string | null,

  birthdateExact: string | null,

  phoneNum: string | null,

  phoneNumExact: boolean,

  hobbies: string[],

  street: string | null,

  streetExact: boolean,

  city: string | null,

  cityExact: boolean,

  country: string | null,

  countryExact: boolean

}
