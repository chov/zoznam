export interface PagingParams {

  offset: number;

  limit: number;

}
