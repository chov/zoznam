import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LoginComponent} from "./login.component";
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserService} from "../services/user.service";

@NgModule({
  declarations: [LoginComponent],
  imports: [CommonModule, MatIconModule, MatButtonModule,
    MatInputModule, MatFormFieldModule, FormsModule, ReactiveFormsModule],
  providers: [UserService],
  exports: [LoginComponent]
})
export class LoginModule {
}
