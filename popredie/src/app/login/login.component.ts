import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {TokenInterface} from "../types/token.interface";
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(private fb: FormBuilder,
              private loginService: UserService,
              private router: Router) {
  }

  loginForm: FormGroup = this.fb.group({
    nameField: ['', Validators.required],
    passwordField: ['', Validators.required]
  });

  hide = true;
  wrongCredentials = false;
  token!: TokenInterface;
  isAdmin!: boolean;

  submitForm() {
    if (!this.loginForm.valid) {
      return;
    }
    this.loginService.login(this.getNameField(), this.getPasswordField()).subscribe(response => {
        this.token = response.accessToken;
        this.isAdmin = response.admin;
        localStorage.setItem('accessToken', this.token.access_token);
        localStorage.setItem('refreshToken', this.token.refresh_token);
        if (this.isAdmin) {
          this.router.navigate(['/admin'])
          return;
        }
        this.router.navigate(['']);
      }, error => {
        this.wrongCredentials = true;
        console.log('error: ' + error.status);
      }
    )
  }

  getNameField(): string {
    return this.loginForm.get('nameField')?.value || '';
  }

  getPasswordField(): string {
    return this.loginForm.get('passwordField')?.value || '';
  }
}
