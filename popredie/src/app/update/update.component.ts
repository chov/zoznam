import {Component, inject} from '@angular/core';
import {ContactInterface} from "../types/contact.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {ContactService} from "../services/contact.service";
import {AddressInterface} from "../types/address.interface";
import {AbstractControl, FormBuilder, ValidatorFn, Validators} from "@angular/forms";
import {MatChipEditedEvent, MatChipInputEvent} from '@angular/material/chips';
import {LiveAnnouncer} from '@angular/cdk/a11y';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent {
  constructor(private activatedRoute: ActivatedRoute,
              private contactService: ContactService,
              private fb: FormBuilder,
              private router: Router) {
  }

  contact: ContactInterface = {
    id: 0,
    age: 0,
    name: '',
    surname: '',
    phoneNum: null,
    birthdate: new Date(),
    image: null,
    hobbies: [],
    address: {
      street: null,
      houseNum: null,
      city: null,
      country: null
    }
  };

  private contactId!: string | null;
  file!: File;
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  hobbies: string[] = [];
  announcer = inject(LiveAnnouncer);

  ngOnInit() {
    this.contactId = (this.activatedRoute.snapshot.paramMap.get('id'));
    this.contactService.getContactById(this.contactId)?.subscribe(resp => {
      this.contact = resp;
      this.updateFieldValues();
      this.hobbies = this.contact.hobbies;
    });
  }

  contactForm = this.fb.group({
    nameField: [this.contact.name, Validators.required],
    surnameField: ['', Validators.required],
    phNumField: ['', Validators.pattern('^(\\+\\d{3}|0)\\d{9}$')],
    birthdateField: ['', [this.pastDateValidator(), Validators.required]],
    imageField: [undefined, this.pngFileValidator()]
  });
  addressForm = this.fb.group({
    streetField: [''],
    cityField: [''],
    countryField: [''],
    houseNumField: [-1]
  })
  parentForm = this.fb.group({
    contactForm: this.contactForm,
    addressForm: this.addressForm
  })

  async submitForm() {
    if (this.contactForm.valid) {
      this.contact = await this.convertToContactInterface();
      this.contact.address = this.convertToAddressInterface();
      this.contactService.updateContact(this.contact).subscribe(resp => {
        if (this.instanceOfContactInterface(resp)) {
          this.contactService.addToUpdateStream(resp);
        }
      });
      this.router.navigate(['/']);
    } else {
      console.log('Form is invalid');
    }
  }

  instanceOfContactInterface(object: any): object is ContactInterface {
    return object;
  }

  returnHome() {
    this.router.navigate(['/']);
  }

  private pastDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const selectedDate = new Date(control.value);
      const currentDate = new Date();

      if (selectedDate < currentDate) {
        return null;
      } else {
        return {
          'pastDate': true
        };
      }
    };
  }

  convertToAddressInterface(): AddressInterface {
    return {
      street: this.addressForm.get('streetField')?.value || null,
      houseNum: this.addressForm.get('houseNumField')?.value || null,
      city: this.addressForm.get('cityField')?.value || null,
      country: this.addressForm.get('countryField')?.value || null
    };
  }

  async convertToContactInterface(): Promise<ContactInterface> {
    try {
      this.contact.image = await this.getBase64(this.file);
    } catch (error) {
      console.error(error);
    }
    this.contact.name = this.contactForm.get('nameField')?.value || '';
    this.contact.surname = this.contactForm.get('surnameField')?.value || '';
    const birthdateControl = this.contactForm.get('birthdateField');
    if (birthdateControl !== null && birthdateControl.value !== null && birthdateControl.value !== undefined) {
      this.contact.birthdate = new Date(birthdateControl.value);
    }
    this.contact.phoneNum = this.contactForm.get('phNumField')?.value || null;
    this.contact.hobbies = this.hobbies;
    return this.contact;
  }

  pngFileValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const file: File = control.value;
      if (control.value === null || control.value === undefined) {
        return null;
      }
      const allowedExtensions = ['png'];
      const fileExtension = file.toString().split('.').pop()?.toLowerCase();

      if (fileExtension && allowedExtensions.indexOf(fileExtension) === -1) {
        return {pngFile: true};
      }
      return null;
    };
  }

  onFilechange(event: any) {
    this.file = event.target.files[0]
  }

  getBase64(file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const reader: FileReader = new FileReader();

      reader.onload = function () {
        if (typeof reader.result === 'string') {
          resolve(reader.result);
        } else {
          reject(new Error('Invalid file type or data.'));
        }
      };

      reader.onerror = function (error) {
        reject(error);
      };

      reader.readAsDataURL(file);
    });
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.hobbies.push(value);
    }
    event.chipInput!.clear();
  }

  remove(hobby: string): void {
    const index = this.hobbies.indexOf(hobby);
    if (index >= 0) {
      this.hobbies.splice(index, 1);
      this.announcer.announce(`Removed ${hobby}`);
    }
  }

  edit(hobby: string, event: MatChipEditedEvent) {
    const value = event.value.trim();
    if (!value) {
      this.remove(hobby);
      return;
    }
    const index = this.hobbies.indexOf(hobby);
    if (index >= 0) {
      this.hobbies[index] = value;
    }
  }

  updateFieldValues() {
    this.contactForm.setValue({
      nameField: this.contact.name,
      surnameField: this.contact.surname,
      birthdateField: this.formatDate(this.contact.birthdate),
      phNumField: this.contact.phoneNum,
      imageField: null,
    });
    this.addressForm.setValue({
      streetField: this.contact.address.street,
      houseNumField: this.contact.address.houseNum,
      cityField: this.contact.address.city,
      countryField: this.contact.address.country
    });
  }

  formatDate(date: Date): string {
    const newDate = new Date(date);
    const year = newDate.getFullYear();
    const month = ('0' + (newDate.getMonth() + 1)).slice(-2);
    const day = ('0' + newDate.getDate()).slice(-2);

    return `${year}-${month}-${day}`;
  }

}
