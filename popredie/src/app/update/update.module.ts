import {NgModule} from "@angular/core";
import {UpdateComponent} from "./update.component";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';



@NgModule({
  declarations: [UpdateComponent],
  imports: [CommonModule, BrowserModule,
    FormsModule, ReactiveFormsModule,
    MatInputModule, MatFormFieldModule, MatIconModule,
  MatChipsModule],
  exports: [UpdateComponent]
})
export class UpdateModule {
}
