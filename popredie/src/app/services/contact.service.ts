import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {BehaviorSubject, Observable, throwError} from "rxjs";
import {catchError, filter} from 'rxjs/operators';
import {ContactInterface} from "../types/contact.interface";
import {RegisterInterface} from "../types/register.interface";
import {PagingParams} from "../params/paging.params";
import {SortingParams} from "../params/sorting.params";
import {FilterParams} from "../params/filter.params";
import {PageResponseInterface} from "../types/page.response.interface";
import {Router} from '@angular/router';
import {AdminPageResponse} from "../types/admin.page.response";
import {FilterParamsAdmin} from "../params/filter.params.admin";
import {UserService} from "./user.service";

@Injectable()
export class ContactService {

  constructor(private http: HttpClient, private router: Router, private userService: UserService) {
  }


  /** Function to retrieve all Contacts from DB
   * @Params offset<number> which page, limit<number> how many contacts per page
   * @Returns List<ContactInterface>*/
  getContacts(pagingParams: PagingParams, sortingParams: SortingParams | null,
              filterParams: FilterParams | null): Observable<PageResponseInterface> {
    const params = this.getParams(pagingParams, sortingParams, filterParams);
    return this.http.get<PageResponseInterface>('http://localhost:8080/contact', this.getHeaderWithParams(params))
      .pipe(
        catchError((error) => this.handleHttpError(error))
      );
  }

  getContactsAdmin(pagingParams: PagingParams, sortingParams: SortingParams | null,
                   filterParams: FilterParamsAdmin | null): Observable<AdminPageResponse> {
    let params = this.getParams(pagingParams, sortingParams, filterParams);
    if (filterParams?.user != null) {
      params = params.set('userCriterion', filterParams.user ?? '');
      params = params.set('userExactCriterion', filterParams.userExact ?? false);
    }
    return this.http.get<AdminPageResponse>('http://localhost:8080/contact/all-contacts', this.getHeaderWithParams(params))
      .pipe(
        catchError((error) => this.handleHttpError(error))
      );
  }

  /** Function to retrieve Contact by its ID
   * @Returns ContactInterface
   * @Input contactId<string | null>: ID of Contact*/
  getContactById(contactId: string | null): Observable<ContactInterface> | null {
    if (contactId == null) {
      return null;
    }
    return this.http.get<ContactInterface>(`http://localhost:8080/contact/${contactId}`, this.getHeader())
      .pipe(
        catchError((error) => this.handleHttpError(error))
      );
  }

  /** Function to delete Contact from DB, will also delete attached Address
   * @Input contactId<string | null>: ID of Contact*/
  deleteContact(contactId: number) {
    const params = new HttpParams().set('contactId', contactId.toString());
    return this.http.delete(`http://localhost:8080/contact`, this.getHeaderWithParams(params))
      .pipe(
        catchError((error) => this.handleHttpError(error))
      );
  }

  /** Function to update Contact, also Address
   * @Input konAdr<ContactInterface>:*/
  updateContact(konAdr: ContactInterface): Observable<Object> {
    return this.http.patch('http://localhost:8080/contact', konAdr, this.getHeader());
  }

  /** Function to add Contact, also Address to DB
   * @Input konAdr<RegisterInterface>:*/
  addContactAddress(contact: RegisterInterface): Observable<Object> {
    return this.http.post('http://localhost:8080/contact', contact, this.getHeader());
  }

  getHeader() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
        'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
      })
    };
  }

  getHeaderWithParams(params: HttpParams) {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
        'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
      }),
      params
    };
  }

  private handleHttpError(error: any) {
    if (error.status === 401) {
      const refreshToken: string | null = localStorage.getItem('refreshToken');
      if (refreshToken) {
        this.userService.refreshAccessToken(refreshToken).subscribe(
          (resp) => {
            localStorage.setItem('accessToken', resp.accessToken.access_token);
            localStorage.setItem('refreshToken', resp.accessToken.refresh_token);
            if (resp.admin) {
              this.router.navigate(['/admin']);
            } else {
              this.router.navigate(['']);
            }
          },
          (error) => {
            console.log('error: ' + error);
            this.router.navigate(['/login']);
          }
        );
      } else {
        this.router.navigate(['/login']);
      }
    }
    return throwError(error);
  }

  getParams(pagingParams: PagingParams, sortingParams: SortingParams | null,
            filterParams: FilterParams | FilterParamsAdmin | null) {
    let params;
    //performs null and undefined checks
    if(filterParams && filterParams.hobbies){
      params = new HttpParams({
        fromObject: { 'hobbies': filterParams.hobbies }})
        .set('offset', pagingParams.offset)
        .set('limit', pagingParams.limit);
    }else {
      params = new HttpParams()
        .set('offset', pagingParams.offset)
        .set('limit', pagingParams.limit);
    }

    if (sortingParams != null && sortingParams.active != undefined) {
      params = params
        .set('sortBy', sortingParams.active)
        .set('direction', sortingParams.direction);
    }

    if (filterParams != null) {
      const invalidValues: (string | boolean | string[] | null)[] = [null, false, true, 'exact', 'later', 'before'];
      for (const value in filterParams) {
        if (value == 'user') {
          continue;
        }
        if(value === 'hobbies'){
          continue;
        }
        const key = value as keyof FilterParams;
        const paramValue = filterParams[key];
        if (!invalidValues.includes(paramValue)) {
          const exactControl = filterParams[key + 'Exact' as keyof FilterParams];
          if (exactControl !== null && !Array.isArray(exactControl)) {
            params = params.set(key + 'ExactCriterion', exactControl);
          }
          if (paramValue != null && !Array.isArray(paramValue)) {
            params = params.set(key + 'Criterion', paramValue);
          }
        }
      }
    }
    return params;
  }

  contactUpdated = new BehaviorSubject<ContactInterface>({
    id: 0,
    name: '',
    surname: '',
    age: 0,
    birthdate: new Date(),
    phoneNum: null,
    image: null,
    hobbies: [],
    address: {
      street: null,
      houseNum: null,
      city: null,
      country: null,
    },
  });

  public updateData$: Observable<ContactInterface> = this.contactUpdated.asObservable();

  addToUpdateStream(contact: ContactInterface) {
    this.contactUpdated.next(contact);
  }

  contactRegistered = new BehaviorSubject<ContactInterface>({
    id: 0,
    name: '',
    surname: '',
    age: 0,
    birthdate: new Date(),
    phoneNum: null,
    image: null,
    hobbies: [],
    address: {
      street: null,
      houseNum: null,
      city: null,
      country: null,
    },
  });
  public registerData$: Observable<ContactInterface> = this.contactRegistered.asObservable();

  addToRegisterStream(contact: ContactInterface) {
    this.contactRegistered.next(contact);
  }

  filterParams = new BehaviorSubject<FilterParams | null>(null);
  public filterData$: Observable<FilterParams | null> = this.filterParams.asObservable().pipe(
    filter((filter) => filter !== null)
  );

  addToFilterStream(filterParam: FilterParams) {
    this.filterParams.next(filterParam);
  }

  adminFilterParams = new BehaviorSubject<FilterParamsAdmin | null>(null);
  public adminFilterData$: Observable<FilterParamsAdmin | null> = this.adminFilterParams.asObservable().pipe(
    filter((filter) => filter !== null)
  );

  addToAdminFilterStream(filterParam: FilterParamsAdmin) {
    this.adminFilterParams.next(filterParam);
  }

}
