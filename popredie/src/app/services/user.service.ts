import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {UserInterface} from "../types/user.interface";
import {LoginResponseInterface} from "../types/login.response.interface";
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';


@Injectable()
export class UserService {
  constructor(private http: HttpClient,
              private router: Router) {
  }

  isAdmin$ = new BehaviorSubject<boolean>(false);

//TODO V REQUESTE SA POSLE HASHNUTY PASSWORD A CHECKNE S HASHNUTYM PASSWORDOM NA KEYCLOAK
  login(name: string, password: string): Observable<LoginResponseInterface> {
    let params = new HttpParams().set('username', name).set('password', password);

    return this.http.get<LoginResponseInterface>('http://localhost:8080/user', {params});
  }

  refreshAccessToken(refreshToken: string): Observable<LoginResponseInterface> {
    const header = new HttpHeaders({
      'refreshToken': refreshToken
    })
    let params = new HttpParams().set('refreshToken', refreshToken);
    return this.http.get<LoginResponseInterface>('http://localhost:8080/user/refresh', {headers: header});
  }

  registerUser(user: UserInterface): Observable<LoginResponseInterface> {
    return this.http.post<LoginResponseInterface>('http://localhost:8080/user', user);
  }

  isUserAdmin(): Observable<boolean> {
    return this.http.get<boolean>('http://localhost:8080/user/is_admin', this.getHeader())
      .pipe(
        catchError((error) => {
          if (error.status === 401) {
            this.router.navigate(['/login']);
          }
          throw error;
        })
      );
  }

  deleteUser() {
    return this.http.delete('http://localhost:8080/user', this.getHeader()).pipe(
      catchError((error) => {
        if (error.status === 401) {
          this.router.navigate(['/login']);
        }
        throw error;
      })
    );
  }

  deleteUserAdmin(user: string) {
    return this.http.delete(`http://localhost:8080/user/${user}`, this.getHeader());
  }

  getHeader(): { headers: HttpHeaders } {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
        'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
      })
    };
  }

}
