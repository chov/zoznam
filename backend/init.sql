create domain telcislo as varchar(15)
    constraint telcislo_check check ((VALUE)::text ~ '^(\+\d{3}|0)\d{9}$'::text);