package com.workflows.backend;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.JPA.AddressServiceJPA;
import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.exceptions.AddressException;
import com.workflows.backend.mapper.AddressMapper;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
class AddressServiceJPATests {

    @Autowired        //AK JE AUTOWIRED TAK PRACUJE S H2 DB
    //@Mock
    private EntityManager entityManager;
    @MockBean
    AddressMapper addressMapper;

    private AddressServiceJPA addressServiceJPA;

    @BeforeEach
    void setUp() {
        this.addressServiceJPA = new AddressServiceJPA(entityManager, addressMapper);
        entityManager.clear();
    }

    @Test
    public void addAddressTest() {
        assertThrows(AddressException.class, () -> addressServiceJPA.addAddress(null));

        Address address = new Address(156789L, new HashSet<>(), "street", 11, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        addressServiceJPA.addAddress(mergedAddress);
        assertTrue(entityManager.contains(mergedAddress));
        //verify(entityManager).persist(address);
        //when(entityManager.find(Address.class, address.getId())).thenReturn(address);
    }

    @Test
    public void getAddressByContactIdTest() {
        when(addressMapper.entityToDto(any(Address.class)))
                .thenAnswer(invocation -> addressEntityToDto(invocation.getArgument(0)));

        assertThrows(AddressException.class, () -> addressServiceJPA.getAddressByContactId(null));

        Address address = new Address(456789L, new HashSet<>(), "street", 11, "city", "country");
        Contact contact = new Contact(123L, "name", "surname", LocalDate.now(), "0123456789", "keycloakId", null, null, null);

        Address mergedAddress = entityManager.merge(address);
        AddressDTO expectedAddress = addressEntityToDto(mergedAddress);
        mergedAddress.getContacts().add(contact);
        contact.setAddress(mergedAddress);
        Contact mergedContact = entityManager.merge(contact);
        AddressDTO retrievedAddress = addressServiceJPA.getAddressByContactId(mergedContact.getId());
        assertTrue(equalsAddress(expectedAddress, retrievedAddress));
    }

    //entityManager.createQuery("SELECT a FROM Address a", Address.class).getResultList()
    @Test
    public void getAddressByIdTest() {
        when(addressMapper.entityToDto(any(Address.class)))
                .thenAnswer(invocation -> addressEntityToDto(invocation.getArgument(0)));

        assertThrows(AddressException.class, () -> addressServiceJPA.getAddressById(null));
        Address address = new Address(456789L, new HashSet<>(), "street", 11, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        AddressDTO expectedAddress = addressEntityToDto(mergedAddress);
        entityManager.persist(mergedAddress);
        entityManager.flush();
        AddressDTO retrievedAddress = addressServiceJPA.getAddressById(mergedAddress.getId());
        assertTrue(equalsAddress(expectedAddress, retrievedAddress));
        //verify(entityManager).find(Address.class, 456789L);
    }

    @Test
    public void getDuplicateAddressTest() {
        Address address = new Address(456789L, new HashSet<>(), "street", 11, "city", "country");

        Optional<Address> retrievedAddress = addressServiceJPA.getDuplicateAddress(address);
        assertFalse(retrievedAddress.isPresent());
        Address mergedAddress = entityManager.merge(address);

        entityManager.persist(mergedAddress);
        Address wrongAddress = new Address(456789L, new HashSet<>(), "WRONGStreet", 11, "city", "country");

        Optional<Address> retrievedWrongAddress = addressServiceJPA.getDuplicateAddress(wrongAddress);
        assertFalse(retrievedWrongAddress.isPresent());

        Optional<Address> retrievedCorrectAddress = addressServiceJPA.getDuplicateAddress(address);
        assertEquals(retrievedCorrectAddress.get(), mergedAddress);
    }

    @Test
    public void deleteAddressWithNoContactsTest() {
        Address address = new Address(456789L, new HashSet<>(), "street", 11, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        entityManager.persist(mergedAddress);

        assertTrue(entityManager.contains(mergedAddress));
        addressServiceJPA.deleteAddressWithNoContacts(mergedAddress);
        assertFalse(entityManager.contains(mergedAddress));

        Contact contact = new Contact(123L, "name", "surname", LocalDate.now(), "0123456789", "keycloakId", null, null, null);
        entityManager.persist(mergedAddress);
        entityManager.merge(mergedAddress);
        mergedAddress.getContacts().add(contact);
        addressServiceJPA.deleteAddressWithNoContacts(mergedAddress);

        assertTrue(entityManager.contains(mergedAddress));
    }

    @Test
    public void deleteAddressTest() {
        Address address = new Address(456789L, new HashSet<>(), "street", 11, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        entityManager.persist(mergedAddress);
        assertTrue(entityManager.contains(mergedAddress));

        addressServiceJPA.deleteAddress(mergedAddress.getId());
        assertFalse(entityManager.contains(mergedAddress));
    }

    private AddressDTO addressEntityToDto(Address address) {
        return new AddressDTO(address.getId(), address.getStreet(), address.getHouseNum(), address.getCity(), address.getCountry());
    }

    public boolean equalsAddress(AddressDTO addressExpected, AddressDTO addressActual) {
        if (addressExpected == addressActual) return true;
        if (addressActual == null || addressExpected.getClass() != addressActual.getClass()) return false;
        return Objects.equals(addressExpected.getId(), addressActual.getId()) &&
                Objects.equals(addressExpected.getStreet(), addressActual.getStreet()) &&
                Objects.equals(addressExpected.getHouseNum(), addressActual.getHouseNum()) &&
                Objects.equals(addressExpected.getCity(), addressActual.getCity()) &&
                Objects.equals(addressExpected.getCountry(), addressActual.getCountry());
    }

}
