package com.workflows.backend;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.DTO.ContactAdminDTO;
import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.JPA.ContactServiceJPA;
import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.exceptions.ContactException;
import com.workflows.backend.mapper.ContactMapper;
import com.workflows.backend.params.PagingParams;
import com.workflows.backend.params.SortingParams;
import com.workflows.backend.params.UserParams;
import com.workflows.backend.response.AdminPageResponse;
import com.workflows.backend.response.PageResponse;
import com.workflows.backend.service.AddressService;
import com.workflows.backend.service.UserService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class ContactServiceJpaTests {
    @Autowired
    //@Mock
    private EntityManager entityManager;

    private ContactServiceJPA contactServiceJPA;

    @Mock
    private ContactMapper contactMapper;

    @Mock
    private AddressService addressService;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        this.contactServiceJPA = new ContactServiceJPA(entityManager, contactMapper, addressService, userService);
        entityManager.clear();
    }

    @Test
    public void getContactByIdTest() {
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        assertThrows(ContactException.class, () -> contactServiceJPA.getContactById(null));
        assertThrows(ContactException.class, () -> contactServiceJPA.getContactById(789L));

        ContactDTO contact = contactEntityToDto(addContactToDb());

        assertThrows(ContactException.class, () -> contactServiceJPA.getContactById(789L));
        assertTrue(equalsContact(contact, contactServiceJPA.getContactById(contact.getId()), true));
    }

    @Test
    public void deleteContactsOfUserTest() {
        assertThrows(ContactException.class, () -> contactServiceJPA.deleteContactsOfUser(null));

        addContactToDb();
        Contact contact = new Contact(null, "name", "surname", LocalDate.now(), "0123456789", "keycloakId2", null, null, null);
        Address address = new Address(null, new HashSet<>(), "street", 1, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        mergedAddress.getContacts().add(contact);
        contact.setAddress(mergedAddress);
        Contact mergedContact = entityManager.merge(contact);

        contactServiceJPA.deleteContactsOfUser("keycloakId");
        String queryString = "SELECT COUNT(c) FROM Contact c";
        TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
        assertEquals(1L, query.getResultList().get(0));
        assertEquals(mergedContact, entityManager.find(Contact.class, mergedContact.getId()));
    }

    @Test
    public void deleteContactTest() {
        assertThrows(ContactException.class, () -> contactServiceJPA.deleteContact(null));

        Contact contact = addContactToDb();
        assertThrows(ContactException.class, () -> contactServiceJPA.deleteContact(789L));  //BAD ID
        assertEquals(entityManager.find(Contact.class, contact.getId()), contact);

        doAnswer(invocation -> {
            entityManager.remove(invocation.getArgument(0));
            return null;
        }).when(addressService).deleteAddressWithNoContacts(any(Address.class));
        contactServiceJPA.deleteContact(contact.getId());
        assertNull(entityManager.find(Contact.class, contact.getId()));
        assertNull(entityManager.find(Address.class, contact.getAddress().getId()));
    }

    @Test
    public void getNumOfAllContactsTest() {
        assertEquals(0L, contactServiceJPA.getNumOfAllContacts());

        Contact contact = addContactToDb();
        assertEquals(1L, contactServiceJPA.getNumOfAllContacts());

        for (int i = 0; i < 5; i++) {
            addContactToDb();
        }
        assertEquals(6L, contactServiceJPA.getNumOfAllContacts());

        entityManager.remove(contact);
        assertEquals(5L, contactServiceJPA.getNumOfAllContacts());
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsUserCriterionTest() {
        when(userService.getUserId("user", true)).thenReturn(List.of("keycloakId2"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(invocation -> contactEntityToAdminDto(invocation.getArgument(0)));
        UserParams userParams = new UserParams("user", true);
        AdminPageResponse pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 52L)), pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 53L)), pageResponse.getContacts().get(1), true));

        when(userService.getUserId("user", false)).thenReturn(List.of("keycloakId2", "keycloakId3"));
        userParams = new UserParams("user", false);
        pageResponse = contactServiceJPA.getPageOfAllContacts(
                new PagingParams(0, 5), null, userParams);
        assertEquals(4, pageResponse.getContacts().size());
        assertTrue(equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 52L)), pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 53L)), pageResponse.getContacts().get(1), true));
        assertTrue(equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 54L)), pageResponse.getContacts().get(2), true));
        assertTrue(equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 55L)), pageResponse.getContacts().get(3), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsLimitTest() {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));
        PageResponse pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 0), null, new UserParams(null, null));
        assertEquals(0, pageResponse.getContacts().size());

        pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 2), null, new UserParams(null, null));
        assertEquals(2, pageResponse.getContacts().size());
        for (ContactDTO contact : pageResponse.getContacts()) {
            assertNotNull(contact);
        }
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsOffsetTest() {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        PageResponse pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 2), null, new UserParams(null, null));
        ContactDTO secondContact = pageResponse.getContacts().get(1);
        pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(1, 1), null, new UserParams(null, null));
        assertEquals(secondContact.getId(), pageResponse.getContacts().get(0).getId());
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init2.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsNameCriterionTest() {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        UserParams userParams = new UserParams(null, null);
        userParams.setNameCriterion("name");
        userParams.setNameExactCriterion(false);
        PageResponse pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 52L)),
                        pageResponse.getContacts().get(1), true));

        userParams.setNameExactCriterion(true);
        pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(1, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(0), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init2.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsBirthdateCriterionTest() {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        assertThrows(ContactException.class, () -> {
            final UserParams userParamss = new UserParams(null, null);
            userParamss.setBirthdateCriterion(LocalDate.now());
            userParamss.setBirthdateExactCriterion("Exact");
            contactServiceJPA.getPageOfContacts
                    (new PagingParams(0, 5), null, userParamss);
        });

        UserParams userParams = new UserParams(null, null);
        userParams.setBirthdateCriterion(LocalDate.now());
        userParams.setBirthdateExactCriterion("exact");
        PageResponse pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 57L)),
                        pageResponse.getContacts().get(1), true));

        when(userService.getUserId()).thenReturn("keycloakId2");
        userParams = new UserParams(null, null);
        userParams.setBirthdateCriterion(LocalDate.now());
        userParams.setBirthdateExactCriterion("before");
        pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 53L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 54L)),
                        pageResponse.getContacts().get(1), true));

        when(userService.getUserId()).thenReturn("keycloakId3");
        userParams = new UserParams(null, null);
        userParams.setBirthdateCriterion(LocalDate.now());
        userParams.setBirthdateExactCriterion("later");
        pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 55L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 56L)),
                        pageResponse.getContacts().get(1), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init2.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsBirthdateNameSortTest() {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        SortingParams sortingParams = new SortingParams("name", "asc");
        UserParams userParams = new UserParams(null, null);
        PageResponse pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 5), sortingParams, userParams);
        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 57L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(1), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 52L)),
                        pageResponse.getContacts().get(2), true));

        sortingParams.setDirection("desc");
        pageResponse = contactServiceJPA.getPageOfContacts
                (new PagingParams(0, 5), sortingParams, userParams);
        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 52L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(1), true));
        assertTrue(
                equalsContact(this.contactEntityToDto(entityManager.find(Contact.class, 57L)),
                        pageResponse.getContacts().get(2), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsLimitTest() {
        when(userService.getUserId(any(), any())).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3", "keycloakId1"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));
        AdminPageResponse pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 0), null, new UserParams(null, null));
        assertEquals(0, pageResponse.getContacts().size());

        pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 5), null, new UserParams(null, null));
        assertEquals(5, pageResponse.getContacts().size());
        for (ContactAdminDTO contact : pageResponse.getContacts()) {
            assertNotNull(contact);
        }
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsOffsetTest() {
        when(userService.getUserId(any(), any())).thenReturn(List.of("keycloakId"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        AdminPageResponse pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 2), null, new UserParams(null, null));
        ContactAdminDTO secondContact = pageResponse.getContacts().get(1);
        pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(1, 1), null, new UserParams(null, null));
        assertEquals(secondContact.getId(), pageResponse.getContacts().get(0).getId());
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsNameCriterionTest() {
        when(userService.getUserId(any(), any())).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3", "keycloakId1"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        UserParams userParams = new UserParams(null, null);
        userParams.setNameCriterion("name");
        userParams.setNameExactCriterion(false);
        AdminPageResponse pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 52L)),
                        pageResponse.getContacts().get(1), true));

        userParams.setNameExactCriterion(true);
        pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(1, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(0), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsBirthdateCriterionTest() {
        when(userService.getUserId(any(), any())).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3", "keycloakId1"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        assertThrows(ContactException.class, () -> {
            final UserParams userParamss = new UserParams(null, null);
            userParamss.setBirthdateCriterion(LocalDate.now());
            userParamss.setBirthdateExactCriterion("Exact");
            contactServiceJPA.getPageOfAllContacts
                    (new PagingParams(0, 5), null, userParamss);
        });

        UserParams userParams = new UserParams(null, null);
        userParams.setBirthdateCriterion(LocalDate.now());
        userParams.setBirthdateExactCriterion("exact");
        AdminPageResponse pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 57L)),
                        pageResponse.getContacts().get(1), true));

        userParams = new UserParams(null, null);
        userParams.setBirthdateCriterion(LocalDate.now());
        userParams.setBirthdateExactCriterion("before");
        pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 52L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 53L)),
                        pageResponse.getContacts().get(1), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 54L)),
                        pageResponse.getContacts().get(2), true));

        userParams = new UserParams(null, null);
        userParams.setBirthdateCriterion(LocalDate.now());
        userParams.setBirthdateExactCriterion("later");
        pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 5), null, userParams);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 55L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 56L)),
                        pageResponse.getContacts().get(1), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsNameSortTest() {
        when(userService.getUserId(any(), any())).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3", "keycloakId1"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        SortingParams sortingParams = new SortingParams("name", "asc");
        UserParams userParams = new UserParams(null, null);
        AdminPageResponse pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 3), sortingParams, userParams);
        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 54L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 53L)),
                        pageResponse.getContacts().get(1), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 55L)),
                        pageResponse.getContacts().get(2), true));

        sortingParams.setDirection("desc");
        pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 3), sortingParams, userParams);
        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 52L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(1), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 57L)),
                        pageResponse.getContacts().get(2), true));

        sortingParams.setDirection("badDirection");
        pageResponse = contactServiceJPA.getPageOfAllContacts
                (new PagingParams(0, 3), sortingParams, userParams);
        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 51L)),
                        pageResponse.getContacts().get(0), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 52L)),
                        pageResponse.getContacts().get(1), true));
        assertTrue(
                equalsContact(this.contactEntityToAdminDto(entityManager.find(Contact.class, 53L)),
                        pageResponse.getContacts().get(2), true));
        assertEquals(51L, pageResponse.getContacts().get(0).getId());
        assertEquals(52L, pageResponse.getContacts().get(1).getId());
        assertEquals(53L, pageResponse.getContacts().get(2).getId());
    }

    @Test
    public void updateContactTest() {
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(invocation -> {
            Contact contact1 = invocation.getArgument(0);
            return contactEntityToDto(contact1);
        });
        when(contactMapper.dtoToEntity(any(ContactDTO.class))).thenAnswer(invocation -> {
            ContactDTO contact1 = invocation.getArgument(0);
            return contactDtoToEntity(contact1);
        });
        doAnswer(invocation -> {
            entityManager.persist(invocation.getArgument(0));
            return null;
        }).when(addressService).addAddress(any(Address.class));
        when(addressService.getDuplicateAddress(any(Address.class))).thenReturn(Optional.empty());
        Contact unchangedContact = addContactToDb();
        Contact shouldChangeContact = addContactToDb();
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(shouldChangeContact.getId(), "test1", "test2", 1, LocalDate.now(), "0123456789", null, null, addressDTO);

        ContactDTO updatedContact = contactServiceJPA.updateContact(contactDTO);
        shouldChangeContact = entityManager.find(Contact.class, updatedContact.getId());

        assertEquals(unchangedContact, entityManager.find(Contact.class, unchangedContact.getId()));

        assertEquals("test1", shouldChangeContact.getName());
        assertEquals("test2", shouldChangeContact.getSurname());
        assertEquals(LocalDate.now(), shouldChangeContact.getBirthdate());
        assertEquals("0123456789", shouldChangeContact.getPhoneNum());
        assertEquals("keycloakId", shouldChangeContact.getKeycloak_id());
        Address shouldChangeAddress = shouldChangeContact.getAddress();
        assertEquals("test1", shouldChangeAddress.getStreet());
        assertEquals("test2", shouldChangeAddress.getCity());
        assertEquals("test3", shouldChangeAddress.getCountry());

        assertEquals(shouldChangeContact.getName(), updatedContact.getName());
        assertEquals(shouldChangeContact.getSurname(), updatedContact.getSurname());
        assertEquals(shouldChangeContact.getBirthdate(), updatedContact.getBirthdate());
        assertEquals(shouldChangeContact.getPhoneNum(), updatedContact.getPhoneNum());
        assertEquals(shouldChangeAddress.getStreet(), updatedContact.getAddress().getStreet());
        assertEquals(shouldChangeAddress.getCity(), updatedContact.getAddress().getCity());
        assertEquals(shouldChangeAddress.getCountry(), updatedContact.getAddress().getCountry());
    }

    @Test
    public void addContactTest() {
        assertThrows(ContactException.class, () -> contactServiceJPA.addContact(null));

        AddressDTO addressDTO = new AddressDTO(null, "street", 1, "city", "country");
        ContactDTO contactDTO = new ContactDTO(null, "name", "surname", 1, LocalDate.now(), "0123456789", null, null, addressDTO);

        Address address = new Address(null, new HashSet<>(), "street", 1, "city", "country");
        Contact contact = new Contact(null, "name", "surname", LocalDate.now(), "0123456789", "keycloakId", null, address, null);
        address.getContacts().add(contact);
        doAnswer(invocation -> {
            entityManager.persist(invocation.getArgument(0));
            return null;
        }).when(addressService).addAddress(any(Address.class));

        when(contactMapper.dtoToEntity(any(ContactDTO.class))).thenReturn(contact);
        when(addressService.getDuplicateAddress(any(Address.class))).thenReturn(Optional.empty());
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(invocation -> {
            Contact contact1 = invocation.getArgument(0);
            return new ContactDTO(contact1.getId(), contact1.getName(), contact1.getSurname(), 1, contact1.getBirthdate(), contact1.getPhoneNum(),
                    null, null, addressEntityToDto(contact1.getAddress()));
        });

        ContactDTO contactDTO1 = contactServiceJPA.addContact(contactDTO);
        assertNotNull(entityManager.find(Contact.class, contactDTO1.getId()));
        Contact retrievedContact = entityManager.find(Contact.class, contactDTO1.getId());
        assertEquals(retrievedContact.getName(), contact.getName());
        assertEquals(retrievedContact.getSurname(), contact.getSurname());
        assertEquals(retrievedContact.getBirthdate(), contact.getBirthdate());
        assertEquals(retrievedContact.getPhoneNum(), contact.getPhoneNum());
        assertEquals(retrievedContact.getKeycloak_id(), contact.getKeycloak_id());
    }

    private AddressDTO addressEntityToDto(Address address) {
        return new AddressDTO(address.getId(), address.getStreet(), address.getHouseNum(), address.getCity(), address.getCountry());
    }

    private ContactDTO contactEntityToDto(Contact contact) {
        ContactDTO contactDTO = new ContactDTO(contact.getId(), contact.getName(), contact.getSurname(), 1, contact.getBirthdate(), contact.getPhoneNum(),
                null, null, addressEntityToDto(contact.getAddress()));
        if (contact.getImage() != null) {
            contactDTO.setImage(new String(Base64.getDecoder().decode(contact.getImage())));
        }
        return contactDTO;
    }

    private Contact contactDtoToEntity(ContactDTO contactDTO) {
        Contact contact = new Contact(contactDTO.getId(), contactDTO.getName(), contactDTO.getSurname(), contactDTO.getBirthdate(), contactDTO.getPhoneNum(), "keycloakId",
                null, addressDtoToEntity(contactDTO.getAddress()), null);
        if (contactDTO.getImage() != null) {
            contact.setImage(Base64.getEncoder().encode(contactDTO.getImage().getBytes(StandardCharsets.UTF_8)));
        }
        return contact;
    }

    private Address addressDtoToEntity(AddressDTO addressDTO) {
        return new Address(addressDTO.getId(), new HashSet<>(), addressDTO.getStreet(), addressDTO.getHouseNum(), addressDTO.getCity(), addressDTO.getCountry());
    }

    private ContactAdminDTO contactEntityToAdminDto(Contact contact) {
        ContactAdminDTO contactAdminDTO = new ContactAdminDTO(contact.getId(), "user", contact.getName(), contact.getSurname(), 1, contact.getBirthdate(), contact.getPhoneNum(),
                null, null, addressEntityToDto(contact.getAddress()));
        if (contact.getImage() != null) {
            contactAdminDTO.setImage(new String(Base64.getDecoder().decode(contact.getImage())));
        }
        return contactAdminDTO;
    }

    private Contact addContactToDb() {
        Contact contact = new Contact(null, "name", "surname", LocalDate.now(), "0123456789", "keycloakId", null, null, Set.of("skiing", "bike"));
        Address address = new Address(null, new HashSet<>(), "street", 1, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        mergedAddress.getContacts().add(contact);
        contact.setAddress(mergedAddress);
        return entityManager.merge(contact);
    }

    public boolean equalsContact(ContactDTO contactExpected, ContactDTO contactActual, boolean contactIdIsSame) {
        if (contactExpected == contactActual) return true;
        if (contactActual == null || contactExpected.getClass() != contactActual.getClass()) return false;
        if (!contactIdIsSame) return Objects.equals(contactExpected.getName(), contactActual.getName()) &&
                Objects.equals(contactExpected.getSurname(), contactActual.getSurname()) &&
                Objects.equals(contactExpected.getBirthdate(), contactActual.getBirthdate()) &&
                Objects.equals(contactExpected.getPhoneNum(), contactActual.getPhoneNum()) &&
                Objects.equals(contactExpected.getAddress().getCity(), contactActual.getAddress().getCity()) &&
                Objects.equals(contactExpected.getAddress().getStreet(), contactActual.getAddress().getStreet()) &&
                Objects.equals(contactExpected.getAddress().getCountry(), contactActual.getAddress().getCountry()) &&
                Objects.equals(contactExpected.getAddress().getHouseNum(), contactActual.getAddress().getHouseNum()) &&
                Objects.equals(contactExpected.getHobbies(), contactActual.getHobbies());

        return Objects.equals(contactExpected.getId(), contactActual.getId()) &&
                Objects.equals(contactExpected.getName(), contactActual.getName()) &&
                Objects.equals(contactExpected.getSurname(), contactActual.getSurname()) &&
                Objects.equals(contactExpected.getBirthdate(), contactActual.getBirthdate()) &&
                Objects.equals(contactExpected.getPhoneNum(), contactActual.getPhoneNum()) &&
                Objects.equals(contactExpected.getAddress().getCity(), contactActual.getAddress().getCity()) &&
                Objects.equals(contactExpected.getAddress().getStreet(), contactActual.getAddress().getStreet()) &&
                Objects.equals(contactExpected.getAddress().getCountry(), contactActual.getAddress().getCountry()) &&
                Objects.equals(contactExpected.getAddress().getHouseNum(), contactActual.getAddress().getHouseNum()) &&
                Objects.equals(contactExpected.getHobbies(), contactActual.getHobbies());
    }
}
