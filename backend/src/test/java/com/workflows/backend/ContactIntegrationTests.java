package com.workflows.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.DTO.ContactAdminDTO;
import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.mapper.ContactMapper;
import com.workflows.backend.response.AdminPageResponse;
import com.workflows.backend.response.PageResponse;
import com.workflows.backend.service.UserService;
import jakarta.persistence.EntityManager;
import jakarta.servlet.ServletContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@ActiveProfiles("test")
@SpringBootTest
@Transactional
public class ContactIntegrationTests {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private EntityManager entityManager;

    @MockBean
    private ContactMapper contactMapper;

    @MockBean
    private UserService userService;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    }

    @Test
    public void verifyConfig() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        //Assert.assertNotNull(webApplicationContext.getBean("ContactController"));
    }

    @Test
    public void getNumOfContactsTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/contact/numOfContacts"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        int numberOfContacts = objectMapper.readValue(responseJson, int.class);
        assertEquals(0, numberOfContacts);

        addContactToDb();
        resultActions = mockMvc.perform(get("/contact/numOfContacts"))
                .andDo(print())
                .andExpect(status().isOk());

        responseJson = resultActions.andReturn().getResponse().getContentAsString();
        numberOfContacts = objectMapper.readValue(responseJson, int.class);
        assertEquals(1, numberOfContacts);
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:reset.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsTest() throws Exception {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        AddressDTO address = new AddressDTO(123L, null, null, "presov", null);
        AddressDTO address2 = new AddressDTO(125L, null, null, "presov", "slovensko");
        ContactDTO contact1 = new ContactDTO(51L, "name", "surname", 0, LocalDate.now(), null, null, Set.of("skiing", "bike"), address);
        ContactDTO contact2 = new ContactDTO(57L, "ename", "bsurname", 0, LocalDate.now(), null, null, Set.of(), address2);
        ResultActions resultActions = mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        PageResponse pageResponse = objectMapper.readValue(responseJson, PageResponse.class);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init3.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsTest() throws Exception {
        when(userService.getUserId(any(String.class), any(Boolean.class))).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        AddressDTO address = new AddressDTO(123L, null, null, "presov", null);
        AddressDTO address2 = new AddressDTO(124L, null, null, "kosice", null);
        AddressDTO address3 = new AddressDTO(125L, null, null, "presov", "slovensko");
        ContactAdminDTO contact1 = new ContactAdminDTO(51L, "keycloakId", "name", "surname", 0, LocalDate.now(), null, null, Set.of("skiing", "bike"), address);
        ContactAdminDTO contact2 = new ContactAdminDTO(52L, "keycloakId", "namea", "asurname", 0, LocalDate.now().minusYears(1), null, null, Set.of("skiing"), address3);
        ContactAdminDTO contact3 = new ContactAdminDTO(53L, "keycloakId2", "bname", "bsurname", 0, LocalDate.now().minusYears(2), null, null, Set.of("bike"), address2);
        ContactAdminDTO contact4 = new ContactAdminDTO(54L, "keycloakId3", "aname", "asurname", 0, LocalDate.now().minusYears(2), null, null, Set.of("football", "basketball"), address3);
        ContactAdminDTO contact5 = new ContactAdminDTO(55L, "keycloakId3", "cname", "bsurname", 0, LocalDate.now().plusYears(1), null, null, Set.of("basketball"), address2);

        ResultActions resultActions = mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        AdminPageResponse pageResponse = objectMapper.readValue(responseJson, AdminPageResponse.class);
        assertEquals(5, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
        assertTrue(equalsContact(contact3, pageResponse.getContacts().get(2), true));
        assertTrue(equalsContact(contact4, pageResponse.getContacts().get(3), true));
        assertTrue(equalsContact(contact5, pageResponse.getContacts().get(4), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init3.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsNameCriterionTest() throws Exception {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("nameCriterion", "name")
                        .param("nameExactCriterion", "badValue"))
                .andDo(print())
                .andExpect(status().is(400));

        mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("nameCriterion", "name"))
                .andDo(print())
                .andExpect(status().is(400));

        AddressDTO address = new AddressDTO(123L, null, null, "presov", null);
        AddressDTO address2 = new AddressDTO(125L, null, null, "presov", "slovensko");
        ContactDTO contact1 = new ContactDTO(51L, "name", "surname", 0, LocalDate.now(), null, null, Set.of("skiing", "bike"), address);
        ContactDTO contact2 = new ContactDTO(52L, "namea", "asurname", 0, LocalDate.now().minusYears(1), null, null, Set.of("skiing"), address2);
        ResultActions resultActions = mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("nameCriterion", "name")
                        .param("nameExactCriterion", "false"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        PageResponse pageResponse = objectMapper.readValue(responseJson, PageResponse.class);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init3.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsNameCriterionTest() throws Exception {
        when(userService.getUserId(any(String.class), any(Boolean.class))).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("nameCriterion", "name")
                        .param("nameExactCriterion", "badValue"))
                .andDo(print())
                .andExpect(status().is(400));

        mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("nameCriterion", "name"))
                .andDo(print())
                .andExpect(status().is(400));

        AddressDTO address = new AddressDTO(123L, null, null, "presov", null);
        AddressDTO address2 = new AddressDTO(125L, null, null, "presov", "slovensko");
        ContactAdminDTO contact1 = new ContactAdminDTO(51L, "keycloak", "name", "surname", 0, LocalDate.now(), null, null, Set.of("skiing", "bike"), address);
        ContactAdminDTO contact2 = new ContactAdminDTO(52L, "keycloak", "namea", "asurname", 0, LocalDate.now().minusYears(1), null, null, Set.of("skiing"), address2);
        ResultActions resultActions = mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("nameCriterion", "name")
                        .param("nameExactCriterion", "false"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        AdminPageResponse pageResponse = objectMapper.readValue(responseJson, AdminPageResponse.class);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
    }

    @Test
    public void getPageOfContactsInvalidBirthdateCriterionTest() throws Exception {
        when(userService.getUserId()).thenReturn("keycloakId");

        mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "badValue"))
                .andDo(print())
                .andExpect(status().is(400));

        mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", "badValue"))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void getPageOfAllContactsInvalidBirthdateCriterionTest() throws Exception {
        when(userService.getUserId(any(String.class), any(Boolean.class))).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3"));

        mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "badValue"))
                .andDo(print())
                .andExpect(status().is(400));

        mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", "badValue"))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init3.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsValidBirthdateCriterionTest() throws Exception {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        AddressDTO address = new AddressDTO(123L, null, null, "presov", null);
        AddressDTO address2 = new AddressDTO(125L, null, null, "presov", "slovensko");
        ContactDTO contact1 = new ContactDTO(51L, "name", "surname", 0, LocalDate.now(), null, null, Set.of("skiing", "bike"), address);
        ContactDTO contact2 = new ContactDTO(57L, "ename", "bsurname", 0, LocalDate.now(), null, null, Set.of(), address2);
        ResultActions resultActions = mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "exact"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        PageResponse pageResponse = objectMapper.readValue(responseJson, PageResponse.class);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));

        contact1 = new ContactDTO(52L, "namea", "asurname", 0, LocalDate.now().minusYears(1), null, null, Set.of("skiing"), address2);
        resultActions = mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "before"))
                .andDo(print())
                .andExpect(status().isOk());
        responseJson = resultActions.andReturn().getResponse().getContentAsString();
        pageResponse = objectMapper.readValue(responseJson, PageResponse.class);

        assertEquals(1, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));

        contact1 = new ContactDTO(56L, "dname", "bsurname", 0, LocalDate.now().plusYears(1), null, null, Set.of(), address2);
        resultActions = mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "later"))
                .andDo(print())
                .andExpect(status().isOk());
        responseJson = resultActions.andReturn().getResponse().getContentAsString();
        pageResponse = objectMapper.readValue(responseJson, PageResponse.class);

        assertEquals(1, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init3.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsValidBirthdateCriterionTest() throws Exception {
        when(userService.getUserId(any(String.class), any(Boolean.class))).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        AddressDTO address = new AddressDTO(123L, null, null, "presov", null);
        AddressDTO address2 = new AddressDTO(125L, null, null, "presov", "slovensko");
        AddressDTO address3 = new AddressDTO(124L, null, null, "kosice", null);
        ContactAdminDTO contact1 = new ContactAdminDTO(51L, "keycloakId", "name", "surname", 0, LocalDate.now(), null, null, Set.of("skiing", "bike"), address);
        ContactAdminDTO contact2 = new ContactAdminDTO(57L, "keycloakId", "ename", "bsurname", 0, LocalDate.now(), null, null, Set.of(), address2);
        ResultActions resultActions = mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "exact"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        AdminPageResponse pageResponse = objectMapper.readValue(responseJson, AdminPageResponse.class);
        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));

        contact1 = new ContactAdminDTO(52L, "keycloakId", "namea", "asurname", 0, LocalDate.now().minusYears(1), null, null, Set.of("skiing"), address2);
        contact2 = new ContactAdminDTO(53L, "keycloakId2", "bname", "bsurname", 0, LocalDate.now().minusYears(2), null, null, Set.of("bike"), address3);
        ContactAdminDTO contact3 = new ContactAdminDTO(54L, "keycloakId3", "aname", "asurname", 0, LocalDate.now().minusYears(2), null, null, Set.of("football", "basketball"), address2);
        resultActions = mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "before"))
                .andDo(print())
                .andExpect(status().isOk());
        responseJson = resultActions.andReturn().getResponse().getContentAsString();
        pageResponse = objectMapper.readValue(responseJson, AdminPageResponse.class);

        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
        assertTrue(equalsContact(contact3, pageResponse.getContacts().get(2), true));

        contact1 = new ContactAdminDTO(55L, "keycloakId3", "cname", "bsurname", 0, LocalDate.now().plusYears(1), null, null, Set.of("basketball"), address3);
        contact2 = new ContactAdminDTO(56L, "keycloakId", "dname", "bsurname", 0, LocalDate.now().plusYears(1), null, null, Set.of(), address2);
        resultActions = mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("birthdateCriterion", LocalDate.now().toString())
                        .param("birthdateExactCriterion", "later"))
                .andDo(print())
                .andExpect(status().isOk());
        responseJson = resultActions.andReturn().getResponse().getContentAsString();
        pageResponse = objectMapper.readValue(responseJson, AdminPageResponse.class);

        assertEquals(2, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init3.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfContactsCountryCriterionTest() throws Exception {
        when(userService.getUserId()).thenReturn("keycloakId");
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));

        AddressDTO address = new AddressDTO(125L, null, null, "presov", "slovensko");
        ContactDTO contact1 = new ContactDTO(52L, "namea", "asurname", 0, LocalDate.now().minusYears(1), null, null, Set.of("skiing"), address);
        ContactDTO contact2 = new ContactDTO(56L, "dname", "bsurname", 0, LocalDate.now().plusYears(1), null, null, Set.of(), address);
        ContactDTO contact3 = new ContactDTO(57L, "ename", "bsurname", 0, LocalDate.now(), null, null, Set.of(), address);

        ResultActions resultActions = mockMvc.perform(get("/contact")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("countryCriterion", "slovensko")
                        .param("countryExactCriterion", "true"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        PageResponse pageResponse = objectMapper.readValue(responseJson, PageResponse.class);
        assertEquals(3, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
        assertTrue(equalsContact(contact3, pageResponse.getContacts().get(2), true));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:init3.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    public void getPageOfAllContactsCountryCriterionTest() throws Exception {
        when(userService.getUserId(any(String.class), any(Boolean.class))).thenReturn(List.of("keycloakId", "keycloakId2", "keycloakId3"));
        when(contactMapper.entityToAdminDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToAdminDto(invocation.getArgument(0)));

        AddressDTO address = new AddressDTO(125L, null, null, "presov", "slovensko");
        ContactAdminDTO contact1 = new ContactAdminDTO(52L, "user", "namea", "asurname", 0, LocalDate.now().minusYears(1), null, null, Set.of("skiing"), address);
        ContactAdminDTO contact2 = new ContactAdminDTO(54L, "user", "aname", "asurname", 0, LocalDate.now().minusYears(2), null, null, Set.of("football", "basketball"), address);
        ContactAdminDTO contact3 = new ContactAdminDTO(56L, "user", "dname", "bsurname", 0, LocalDate.now().plusYears(1), null, null, Set.of(), address);
        ContactAdminDTO contact4 = new ContactAdminDTO(57L, "user", "ename", "bsurname", 0, LocalDate.now(), null, null, Set.of(), address);

        ResultActions resultActions = mockMvc.perform(get("/contact/all-contacts")
                        .param("limit", "5")
                        .param("offset", "0")
                        .param("countryCriterion", "slovensko")
                        .param("countryExactCriterion", "true"))
                .andDo(print())
                .andExpect(status().isOk());

        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        AdminPageResponse pageResponse = objectMapper.readValue(responseJson, AdminPageResponse.class);
        assertEquals(4, pageResponse.getContacts().size());
        assertTrue(equalsContact(contact1, pageResponse.getContacts().get(0), true));
        assertTrue(equalsContact(contact2, pageResponse.getContacts().get(1), true));
        assertTrue(equalsContact(contact3, pageResponse.getContacts().get(2), true));
        assertTrue(equalsContact(contact4, pageResponse.getContacts().get(3), true));
    }

    @Test
    public void getContactByIdTest() throws Exception {
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));
        Contact contact = addContactToDb();
        ContactDTO expectContact = contactEntityToDto(contact);

        mockMvc.perform(get("/contact/5643"))   //BAD ID
                .andExpect(status().is(400));

        ResultActions resultActions = mockMvc.perform(get("/contact/" + contact.getId().toString()))
                .andDo(print())
                .andExpect(status().isOk());
        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        ContactDTO actualContact = objectMapper.readValue(responseJson, ContactDTO.class);

        assertTrue(equalsContact(expectContact, actualContact, true));
    }

    @Test
    public void deleteContactTest() throws Exception {
        Contact contact = addContactToDb();

        mockMvc.perform(delete("/contact")
                        .param("contactId", ""))
                .andDo(print())
                .andExpect(status().is(400));

        mockMvc.perform(delete("/contact")
                        .param("contactId", "8948"))    //BAD ID
                .andDo(print())
                .andExpect(status().is(400));

        mockMvc.perform(delete("/contact")
                        .param("contactId", contact.getId().toString()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void addContact() throws Exception {
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));
        when(contactMapper.dtoToEntity(any(ContactDTO.class))).thenAnswer(
                invocation -> contactDtoToEntity(invocation.getArgument(0)));

        addContactToDb();
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(null, "test1", "test2", 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);

        String requestJson = objectMapper.writeValueAsString(contactDTO);
        ResultActions resultActions = mockMvc.perform(post("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk());
        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        ContactDTO returnedContact = objectMapper.readValue(responseJson, ContactDTO.class);
        ContactDTO actualContact = contactEntityToDto(entityManager.find(Contact.class, returnedContact.getId()));

        assertTrue(equalsContact(contactDTO, returnedContact, false));
        assertTrue(equalsContact(returnedContact, actualContact, true));
    }

    @Test
    public void addContactBadNameParam() throws Exception {
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(null, null, "test2", 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);

        String requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(post("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));

        contactDTO = new ContactDTO(null, "", "test2", 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);
        requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(post("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void addContactBadBirthdateTest() throws Exception {
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(null, "test1", "test2", 1, LocalDate.now().plusDays(1), "0123456789", null, null, addressDTO);

        String requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(post("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void addContactBadAddressHouseNumTest() throws Exception {
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));
        when(contactMapper.dtoToEntity(any(ContactDTO.class))).thenAnswer(
                invocation -> contactDtoToEntity(invocation.getArgument(0)));

        AddressDTO addressDTO = new AddressDTO(null, "test1", 0, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(null, "test1", "test2", 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);

        String requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(post("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void updateContactBadSurnameParam() throws Exception {
        Contact contact = addContactToDb();
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(contact.getId(), "test1", null, 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);

        String requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(patch("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));

        contactDTO = new ContactDTO(contact.getId(), "test1", "", 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);
        requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(patch("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void updateContactAgeParamTest() throws Exception {
        Contact contact = addContactToDb();
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");

        ContactDTO contactDTO = new ContactDTO(contact.getId(), "test1", "test2", -1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);
        String requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(patch("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void updateContactBadBirthdateTest() throws Exception {
        Contact contact = addContactToDb();
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(contact.getId(), "test1", "test2", 1, LocalDate.now().plusDays(1), "0123456789", null, null, addressDTO);

        String requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(patch("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void updateContactBadAddressTest() throws Exception {
        Contact contact = addContactToDb();
        ContactDTO contactDTO = new ContactDTO(contact.getId(), "test1", "test2", 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, null);

        String requestJson = objectMapper.writeValueAsString(contactDTO);
        mockMvc.perform(patch("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    public void updateContactTest() throws Exception {
        when(contactMapper.entityToDto(any(Contact.class))).thenAnswer(
                invocation -> contactEntityToDto(invocation.getArgument(0)));
        when(contactMapper.dtoToEntity(any(ContactDTO.class))).thenAnswer(
                invocation -> contactDtoToEntity(invocation.getArgument(0)));

        Contact shouldChangeContact = addContactToDb();
        Contact unchangedContact = addContactToDb();
        AddressDTO addressDTO = new AddressDTO(null, "test1", 1, "test2", "test3");
        ContactDTO contactDTO = new ContactDTO(shouldChangeContact.getId(), "test1", "test2", 1, LocalDate.ofYearDay(2000, 156), "0123456789", null, null, addressDTO);

        String requestJson = objectMapper.writeValueAsString(contactDTO);

        ResultActions resultActions = mockMvc.perform(patch("/contact")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andExpect(status().isOk());
        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        ContactDTO returnedContact = objectMapper.readValue(responseJson, ContactDTO.class);
        ContactDTO actualContact = contactEntityToDto(entityManager.find(Contact.class, shouldChangeContact.getId()));

        assertTrue(equalsContact(contactDTO, returnedContact, true));
        assertTrue(equalsContact(returnedContact, actualContact, true));
        assertTrue(equalsContact(contactEntityToDto(unchangedContact), contactEntityToDto(entityManager.find(Contact.class, unchangedContact.getId())), true));
    }

    private Contact addContactToDb() {
        Contact contact = new Contact(null, "name", "surname", LocalDate.ofYearDay(2000, 156), "0123456789", "keycloakId", null, null, Set.of("skiing", "bike"));
        Address address = new Address(null, new HashSet<>(), "street", 1, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        mergedAddress.getContacts().add(contact);
        contact.setAddress(mergedAddress);
        return entityManager.merge(contact);
    }

    private AddressDTO addressEntityToDto(Address address) {
        return new AddressDTO(address.getId(), address.getStreet(), address.getHouseNum(), address.getCity(), address.getCountry());
    }

    private ContactDTO contactEntityToDto(Contact contact) {
        ContactDTO contactDTO = new ContactDTO(contact.getId(), contact.getName(), contact.getSurname(), 0, contact.getBirthdate(), contact.getPhoneNum(),
                null, contact.getHobbies(), addressEntityToDto(contact.getAddress()));
        if (contact.getImage() != null) {
            contactDTO.setImage(new String(Base64.getDecoder().decode(contact.getImage())));
        }
        return contactDTO;
    }

    private Contact contactDtoToEntity(ContactDTO contactDTO) {
        Contact contact = new Contact(contactDTO.getId(), contactDTO.getName(), contactDTO.getSurname(), contactDTO.getBirthdate(), contactDTO.getPhoneNum(), "keycloakId",
                null, addressDtoToEntity(contactDTO.getAddress()), contactDTO.getHobbies());
        if (contactDTO.getImage() != null) {
            contact.setImage(Base64.getEncoder().encode(contactDTO.getImage().getBytes(StandardCharsets.UTF_8)));
        }
        return contact;
    }

    private Address addressDtoToEntity(AddressDTO addressDTO) {
        return new Address(addressDTO.getId(), new HashSet<>(), addressDTO.getStreet(), addressDTO.getHouseNum(), addressDTO.getCity(), addressDTO.getCountry());
    }

    private ContactAdminDTO contactEntityToAdminDto(Contact contact) {
        ContactAdminDTO contactAdminDTO = new ContactAdminDTO(contact.getId(), "user", contact.getName(), contact.getSurname(), 0, contact.getBirthdate(), contact.getPhoneNum(),
                null, contact.getHobbies(), addressEntityToDto(contact.getAddress()));
        if (contact.getImage() != null) {
            contactAdminDTO.setImage(new String(Base64.getDecoder().decode(contact.getImage())));
        }
        return contactAdminDTO;
    }

    public boolean equalsContact(ContactDTO contactExpected, ContactDTO contactActual, boolean contactIdIsSame) {
        if (contactExpected == contactActual) return true;
        if (contactActual == null || contactExpected.getClass() != contactActual.getClass()) return false;
        if (!contactIdIsSame) return Objects.equals(contactExpected.getName(), contactActual.getName()) &&
                Objects.equals(contactExpected.getSurname(), contactActual.getSurname()) &&
                Objects.equals(contactExpected.getBirthdate(), contactActual.getBirthdate()) &&
                Objects.equals(contactExpected.getPhoneNum(), contactActual.getPhoneNum()) &&
                Objects.equals(contactExpected.getAddress().getCity(), contactActual.getAddress().getCity()) &&
                Objects.equals(contactExpected.getAddress().getStreet(), contactActual.getAddress().getStreet()) &&
                Objects.equals(contactExpected.getAddress().getCountry(), contactActual.getAddress().getCountry()) &&
                Objects.equals(contactExpected.getAddress().getHouseNum(), contactActual.getAddress().getHouseNum()) &&
                Objects.equals(contactExpected.getHobbies(), contactActual.getHobbies());

        return Objects.equals(contactExpected.getId(), contactActual.getId()) &&
                Objects.equals(contactExpected.getName(), contactActual.getName()) &&
                Objects.equals(contactExpected.getSurname(), contactActual.getSurname()) &&
                Objects.equals(contactExpected.getBirthdate(), contactActual.getBirthdate()) &&
                Objects.equals(contactExpected.getPhoneNum(), contactActual.getPhoneNum()) &&
                Objects.equals(contactExpected.getAddress().getCity(), contactActual.getAddress().getCity()) &&
                Objects.equals(contactExpected.getAddress().getStreet(), contactActual.getAddress().getStreet()) &&
                Objects.equals(contactExpected.getAddress().getCountry(), contactActual.getAddress().getCountry()) &&
                Objects.equals(contactExpected.getAddress().getHouseNum(), contactActual.getAddress().getHouseNum()) &&
                Objects.equals(contactExpected.getHobbies(), contactActual.getHobbies());
    }
}