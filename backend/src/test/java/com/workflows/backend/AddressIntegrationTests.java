package com.workflows.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import jakarta.persistence.EntityManager;
import jakarta.servlet.ServletContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@ActiveProfiles("test")
@SpringBootTest
@Transactional
public class AddressIntegrationTests {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    }

    @Test
    public void verifyConfig() {
        ServletContext servletContext = webApplicationContext.getServletContext();

        assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        //Assert.assertNotNull(webApplicationContext.getBean("ContactController"));
    }

    @Test
    public void getAddressByContactIdTest() throws Exception {
        mockMvc.perform(get("/address/by_contact_id/"))
                .andExpect(status().is(404));
        mockMvc.perform(get("/address/by_contact_id/48794"))
                .andExpect(status().is(400));

        Contact contact = addContactToDb();
        AddressDTO expectedContact = addressEntityToDto(contact.getAddress());
        ResultActions resultActions = mockMvc.perform(get("/address/by_contact_id/" + contact.getId().toString()))
                .andExpect(status().isOk());
        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        AddressDTO returnedAddress = objectMapper.readValue(responseJson, AddressDTO.class);

        Assertions.assertTrue(equalsAddress(expectedContact, returnedAddress));
    }

    @Test
    public void deleteAddressTest() throws Exception {
        mockMvc.perform(delete("/address"))
                .andDo(print())
                .andExpect(status().is(400));
        mockMvc.perform(delete("/address")
                        .param("addressId", "894L"))
                .andExpect(status().is(400));

        Address address = addAddressToDb();
        mockMvc.perform(delete("/address")
                        .param("addressId", address.getId().toString()))
                .andExpect(status().is(200));

        assertFalse(entityManager.contains(address));
    }

    @Test
    public void getAddressTest() throws Exception {
        mockMvc.perform(get("/address"))
                .andDo(print())
                .andExpect(status().is(400));
        mockMvc.perform(get("/address")
                        .param("addressId", "894L"))
                .andExpect(status().is(400));

        Address address = addAddressToDb();
        AddressDTO expectedAddress = addressEntityToDto(address);
        ResultActions resultActions = mockMvc.perform(get("/address")
                        .param("addressId", address.getId().toString()))
                .andExpect(status().is(200));
        String responseJson = resultActions.andReturn().getResponse().getContentAsString();
        AddressDTO returnedAddress = objectMapper.readValue(responseJson, AddressDTO.class);

        assertTrue(equalsAddress(expectedAddress, returnedAddress));
    }


    private Contact addContactToDb() {
        Contact contact = new Contact(null, "name", "surname", LocalDate.ofYearDay(2000, 156), "0123456789", "keycloakId", null, null, null);
        Address address = new Address(null, new HashSet<>(), "street", 1, "city", "country");
        Address mergedAddress = entityManager.merge(address);
        mergedAddress.getContacts().add(contact);
        contact.setAddress(mergedAddress);
        return entityManager.merge(contact);
    }

    private Address addAddressToDb() {
        Address address = new Address(null, new HashSet<>(), "street", 1, "city", "country");
        return entityManager.merge(address);
    }

    private AddressDTO addressEntityToDto(Address address) {
        return new AddressDTO(address.getId(), address.getStreet(), address.getHouseNum(), address.getCity(), address.getCountry());
    }

    public boolean equalsAddress(AddressDTO addressExpected, AddressDTO addressActual) {
        if (addressExpected == addressActual) return true;
        if (addressActual == null || addressExpected.getClass() != addressActual.getClass()) return false;
        return Objects.equals(addressExpected.getId(), addressActual.getId()) &&
                Objects.equals(addressExpected.getStreet(), addressActual.getStreet()) &&
                Objects.equals(addressExpected.getHouseNum(), addressActual.getHouseNum()) &&
                Objects.equals(addressExpected.getCity(), addressActual.getCity()) &&
                Objects.equals(addressExpected.getCountry(), addressActual.getCountry());
    }

}
