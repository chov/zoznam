insert into address(id, city) values(123, 'presov');
insert into address(id, city) values(124, 'kosice');
insert into address(id, city, country) values(125, 'presov', 'slovensko');
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
    values(51, 'name', 'surname', current_date, 'keycloakId', 123);
insert into hobby (contact_id, name) values (51, 'skiing');
insert into hobby (contact_id, name) values (51, 'bike');
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
    values(52, 'namea', 'asurname', DATEADD('YEAR', -1, current_date), 'keycloakId2', 123);
insert into hobby (contact_id, name) values (52, 'skiing');
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
    values(53, 'bname', 'bsurname', DATEADD('YEAR', -2, current_date), 'keycloakId2', 124);
insert into hobby (contact_id, name) values (53, 'bike');
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
    values(54, 'aname', 'asurname', DATEADD('YEAR', -2, current_date), 'keycloakId3', 125);
insert into hobby (contact_id, name) values (54, 'football');
insert into hobby (contact_id, name) values (54, 'basketball');
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
    values(55, 'cname', 'bsurname', DATEADD('YEAR', 1, current_date), 'keycloakId3', 124);
insert into hobby (contact_id, name) values (55, 'basketball');
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
    values(56, 'dname', 'bsurname', DATEADD('YEAR', 1, current_date), 'keycloakId1', 125);
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
    values(57, 'ename', 'bsurname', current_date, 'keycloakId', 125);