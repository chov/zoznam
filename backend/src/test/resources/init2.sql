insert into address(id, city) values(123, 'presov');
insert into address(id, city) values(124, 'kosice');
insert into address(id, city, country) values(125, 'presov', 'slovensko');
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
values(51, 'name', 'surname', current_date, 'keycloakId', 123);
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
values(52, 'namea', 'asurname', DATEADD('YEAR', -1, current_date), 'keycloakId', 123);
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
values(53, 'bname', 'bsurname', DATEADD('YEAR', -2, current_date), 'keycloakId2', 124);
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
values(54, 'aname', 'asurname', DATEADD('YEAR', -2, current_date), 'keycloakId2', 125);
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
values(55, 'bname', 'bsurname', DATEADD('YEAR', 1, current_date), 'keycloakId3', 124);
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
values(56, 'cname', 'bsurname', DATEADD('YEAR', 1, current_date), 'keycloakId3', 125);
insert into contact(id, name, surname, birthdate, keycloak_id, address_id)
values(57, 'cname', 'bsurname', current_date, 'keycloakId', 125);