package com.workflows.backend.service;

import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.exceptions.ContactException;
import com.workflows.backend.params.FilterParams;
import com.workflows.backend.params.PagingParams;
import com.workflows.backend.params.SortingParams;
import com.workflows.backend.params.UserParams;
import com.workflows.backend.response.AdminPageResponse;
import com.workflows.backend.response.PageResponse;
import org.springframework.http.ResponseEntity;

public interface ContactService {

    /**
     * Persists contact to database
     *
     * @param contactDTO Contact to be persisted, in form of DTO
     * @return Persisted contact, now with id, also in form of DTO
     */
    ContactDTO addContact(ContactDTO contactDTO) throws ContactException;

    /**
     * Returns page of contacts based on provided parameters, response includes only contacts of user from who request came from
     *
     * @param pagingParams  Defines size and number of page
     * @param sortingParams Defines by what and in what order query should sort
     * @param filterParams  Defines criteria by which query should filter contacts
     * @return PageResponse consists of list of contacts in form of DTO, pageResponse also has number of all filtered contacts
     */
    PageResponse getPageOfContacts(PagingParams pagingParams, SortingParams sortingParams, FilterParams filterParams) throws ContactException;

    /**
     * Returns number of all contacts from all users
     */
    int getNumOfAllContacts() throws ContactException;

    /**
     * Deletes contact by its id, also deletes address from the contact, if it's not associated with any other contact
     *
     * @param contactId id of contact to be deleted
     */
    ResponseEntity<String> deleteContact(Long contactId) throws ContactException;

    /**
     * Searches for contact by its id
     *
     * @param contactId id of contact
     * @return Contact having provided id
     */
    ContactDTO getContactById(Long contactId) throws ContactException;

    /**
     * Deletes all contacts associated with the user
     *
     * @param keycloakId id of user, whose contacts should be deleted
     */
    void deleteContactsOfUser(String keycloakId);

    /**
     * Updates contacts values (replaces values of contact having same id)
     *
     * @param contactDTO contact with updated values, in form of DTO
     * @return Returns updated contact, in form of DTO
     */
    ContactDTO updateContact(ContactDTO contactDTO) throws ContactException;

    /**
     * Returns page of contacts based on provided parameters, response includes contacts from all users
     *
     * @param pagingParams  Defines size and number of page
     * @param sortingParams Defines by what and in what order query should sort
     * @param userParams    Defines criteria by which query should filter contacts
     * @return PageResponse consists of list of contacts in form of DTO,
     * contacts have additional value (user with whom contact is associated),
     * pageResponse also has number of all filtered contacts
     */
    AdminPageResponse getPageOfAllContacts(PagingParams pagingParams, SortingParams sortingParams, UserParams userParams);
}
