package com.workflows.backend.service;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.entity.Address;
import com.workflows.backend.exceptions.AddressException;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;

public interface AddressService {

    /**
     * Persists address to database, also assign id to it
     *
     * @param address address to be persisted
     */
    void addAddress(Address address) throws AddressException;

    /**
     * Finds address by its contact id
     *
     * @param contactId id of the contact
     * @return address associated with contactId (contact have only one address)
     */
    AddressDTO getAddressByContactId(Long contactId) throws AddressException;

    /**
     * Finds duplicate address (if it exists), recommended to use before persisting
     *
     * @param address Possibly already persisted address
     * @return Optional<Address> Returns duplicate already persisted address, if its present
     */
    Optional<Address> getDuplicateAddress(Address address);

    /**
     * Deletes address, if no contact is associated with it
     *
     * @param address Address that will be possibly deleted
     */
    void deleteAddressWithNoContacts(Address address);

    /**
     * Deletes address by its id
     *
     * @param addressId id of the address
     */
    void deleteAddress(Long addressId) throws DataIntegrityViolationException, AddressException;

    /**
     * Finds address by its id
     *
     * @param addressId id of the address
     * @return address associated with the id
     */
    AddressDTO getAddressById(Long addressId);
}