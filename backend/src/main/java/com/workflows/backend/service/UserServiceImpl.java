package com.workflows.backend.service;

import com.nimbusds.jose.shaded.gson.internal.LinkedTreeMap;
import com.workflows.backend.DTO.UserDTO;
import com.workflows.backend.exceptions.DeleteUserException;
import com.workflows.backend.exceptions.RefreshException;
import com.workflows.backend.exceptions.RegistrationException;
import com.workflows.backend.response.AccessTokenResp;
import com.workflows.backend.response.DeleteResponse;
import com.workflows.backend.response.RegisterResponse;
import com.workflows.backend.security.Credentials;
import com.workflows.backend.security.KeycloakConfig;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import org.apache.http.HttpStatus;
import org.keycloak.TokenVerifier;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final KeycloakConfig keycloakConfig;

    @Override
    public AccessTokenResp refreshAccessToken(String refreshToken) throws RefreshException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("client_id", keycloakConfig.getClientId());
        requestBody.add("client_secret", keycloakConfig.getClientSecret());
        requestBody.add("refresh_token", refreshToken);
        requestBody.add("grant_type", "refresh_token");

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(requestBody, headers);
        try {
            ResponseEntity<AccessTokenResponse> response = restTemplate.exchange(keycloakConfig.getServerUrl() + "/realms/" + keycloakConfig.getRealm() + "/protocol/openid-connect/token",
                    HttpMethod.POST, httpEntity, AccessTokenResponse.class);
            if (response.getBody() == null) {
                throw new RefreshException("Unexpected error occurred", 500);
            }
            if (!response.getStatusCode().is2xxSuccessful()) {
                throw new RefreshException(response.getBody().toString(), response.getStatusCode().value());
            }
            boolean isAdmin;
            try {
                isAdmin = TokenVerifier.create(response.getBody().getToken(), AccessToken.class)
                        .getToken()
                        .getResourceAccess()
                        .get(keycloakConfig.getClientId())
                        .getRoles()
                        .contains("admin");
            } catch (Exception e) {
                isAdmin = false;
            }
            return new AccessTokenResp(response.getBody(), isAdmin);
        } catch (RefreshException exception) {
            throw exception;
        } catch (HttpClientErrorException exception) {
            System.out.println("abort");
            throw new RefreshException(exception.getMessage(), exception, exception.getStatusCode().value());
        } catch (Exception e) {
            throw new RefreshException(e.getMessage(), e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public AccessTokenResp login(UserDTO userDTO) {
        Keycloak instance = Keycloak.getInstance(keycloakConfig.getServerUrl(), keycloakConfig.getRealm(),
                userDTO.getUsername(), userDTO.getPassword(), keycloakConfig.getClientId(), keycloakConfig.getClientSecret());
        TokenManager tokenManager = instance.tokenManager();

        boolean isAdmin;
        try {
            isAdmin = TokenVerifier
                    .create(tokenManager.getAccessTokenString(), AccessToken.class)
                    .getToken()
                    .getResourceAccess()
                    .get(keycloakConfig.getClientId())
                    .getRoles()
                    .contains("admin");
        } catch (Exception e) {
            isAdmin = false;
        }
        instance.close();
        return new AccessTokenResp(tokenManager.getAccessToken(), isAdmin);
    }

    @Override
    public String getUserId() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return null;
        }
        Jwt keycloakToken = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return keycloakToken.getClaimAsString("sub");
    }

    @Override
    public List<String> getUserId(String user, Boolean exact) {
        Jwt keycloakToken = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Keycloak instance = Keycloak.getInstance(keycloakConfig.getServerUrl(), keycloakConfig.getRealm(), keycloakConfig.getClientId(), keycloakToken.getTokenValue());
        List<String> userList = instance.realm(keycloakConfig.getRealm())
                .users()
                .search(user, exact)
                .stream()
                .map(UserRepresentation::getId)
                .toList();
        instance.close();
        return userList;
    }

    @Override
    public String getUserName(String keycloakId) {
        Keycloak instance = keycloakConfig.getInstance();
        UserRepresentation user = instance.realm(keycloakConfig.getRealm())
                .users()
                .get(keycloakId)
                .toRepresentation();
        return user.getUsername();
    }

    @Override
    public boolean getIsAdmin() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return false;
        }
        Jwt keycloakToken = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Map<String, Object> map = keycloakToken.getClaimAsMap("resource_access");
        if (map.containsKey(keycloakConfig.getClientId())) {
            return ((ArrayList<?>) ((LinkedTreeMap<?, ?>) map.get(keycloakConfig.getClientId())).get("roles")).contains("admin");
        }
        return false;
    }

    @Override
    public ResponseEntity<AccessTokenResp> registerUser(UserDTO userDTO) throws RegistrationException {
        CredentialRepresentation credential = Credentials.createPasswordCredentials(userDTO.getPassword());
        UserRepresentation user = new UserRepresentation();
        user.setUsername(userDTO.getUsername());
        user.setRequiredActions(Collections.emptyList());
        user.setCredentials(Collections.singletonList(credential));
        user.setEnabled(true);

        Keycloak instance = keycloakConfig.getInstance();
        UsersResource usersResource = instance.realm(keycloakConfig.getRealm()).users();

        try (Response response = usersResource.create(user)) {
            response.bufferEntity();

            if (response.getStatus() == HttpStatus.SC_CREATED) {
                System.out.println("Created");
                return RegisterResponse.handleResponse(login(userDTO));
            } else {
                throw new RegistrationException(response.readEntity(String.class), response.getStatus());
            }
        } catch (RegistrationException exception) {
            throw exception;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RegistrationException(e.getMessage(), e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> deleteUser(String userId) {
        Keycloak instance = keycloakConfig.getInstance();
        UsersResource usersResource = instance.realm(keycloakConfig.getRealm()).users();

        try (Response response = usersResource.delete(userId)) {
            response.bufferEntity();
            if (response.getStatus() == HttpStatus.SC_NO_CONTENT) {
                System.out.println("DELETED");
                return DeleteResponse.handleResponse();
            } else {
                throw new DeleteUserException(response.readEntity(String.class), response.getStatus());
            }
        } catch (DeleteUserException exception) {
            throw exception;
        } catch (Exception e) {
            throw new DeleteUserException(e.getMessage(), e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }
}