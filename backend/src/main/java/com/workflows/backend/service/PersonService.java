package com.workflows.backend.service;

import com.workflows.backend.DTO.PersonDTO;

import java.util.List;

public interface PersonService {

    /**
     * Persists passed person into database
     *
     * @param person person to be persisted, in form of DTO (should not have ID, as it's generated)
     * @return persisted person, in form of DTO
     */
    PersonDTO addPerson(PersonDTO person);

    /**
     * Method to retrieve all persons from database
     *
     * @return list<PersonDTO> list of all persons
     */
    List<PersonDTO> getAllPersons();

    /**
     * Method to retrieve person based on its id in database
     *
     * @param personId identifier of person (table => person, column => id)
     * @return person in form of DTO
     */
    PersonDTO getPerson(Long personId);

    /**
     * Updates person data
     *
     * @param personDTO person with updated data
     * @return person retrieved from database with already updated data
     */
    PersonDTO updatePerson(PersonDTO personDTO);

    /**
     * Method to delete person from database
     *
     * @param personId identifier of person that should be deleted
     */
    void deletePerson(Long personId);
}
