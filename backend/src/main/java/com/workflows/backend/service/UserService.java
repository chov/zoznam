package com.workflows.backend.service;

import com.workflows.backend.DTO.UserDTO;
import com.workflows.backend.exceptions.RefreshException;
import com.workflows.backend.exceptions.RegistrationException;
import com.workflows.backend.response.AccessTokenResp;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    AccessTokenResp refreshAccessToken(String refreshToken) throws RefreshException;

    /**
     * Searches for user based on provided credentials, if successful users access token is returned
     *
     * @param userDTO users credentials, consists of username and password
     * @return LoginResponse consists of Access token response and boolean value if user is admin
     */
    AccessTokenResp login(UserDTO userDTO);

    /**
     * Returns users (keycloak) id that is held at SecurityContextHolder obtained from clients request
     *
     * @return String user(keycloak) id
     */
    String getUserId();

    /**
     * Checks if the user is admin based on SecurityContextHolder obtained from clients request
     *
     * @return boolean value, if true user is admin
     */
    boolean getIsAdmin();

    /**
     * Registers new user with desired credentials,
     * if user with same username already exists, RegistrationException is thrown
     *
     * @param userDTO desired credentials
     * @return ResponseEntity: consists of response status and LoginResponse for immediate clients login
     */
    ResponseEntity<AccessTokenResp> registerUser(UserDTO userDTO) throws RegistrationException;

    /**
     * Finds users name based on keycloak id associated with it
     */
    String getUserName(String keycloakId);

    /**
     * Searches for users based on provided parameters
     *
     * @param user  String value of users name
     * @param exact boolean value if users name should be exact same as provided in @param user,
     *              if false, users name is case-insensitive and can also be substring of the whole name
     * @return List of usernames whose meets provided parameters
     */
    List<String> getUserId(String user, Boolean exact);

    /**
     * Deletes user based on its keycloak id
     *
     * @param userId users (keycloak) id
     * @return ResponseEntity consists of response status (204 if successful) and optional message
     */
    ResponseEntity<String> deleteUser(String userId);
}