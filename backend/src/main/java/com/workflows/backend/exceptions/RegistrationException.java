package com.workflows.backend.exceptions;

import lombok.Getter;

import java.io.Serial;

@Getter
public class RegistrationException extends Exception {
    @Serial
    private static final long serialVersionUID = -8962561573974135643L;
    private int status = -1;

    public RegistrationException(String message, int status) {
        super(message);
        this.status = status;
    }

    public RegistrationException(String message, Throwable cause, int status) {
        super(message, cause);
        this.status = status;
    }
}