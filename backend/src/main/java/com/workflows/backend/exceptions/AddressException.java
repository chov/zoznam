package com.workflows.backend.exceptions;

import java.io.Serial;

public class AddressException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 2548515369233846089L;

    public AddressException(String message){
        super(message);
    }

    public AddressException(String message, Throwable cause) {
        super(message, cause);
    }
}