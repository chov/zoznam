package com.workflows.backend.exceptions;

import lombok.Getter;

import java.io.Serial;

@Getter
public class RefreshException extends Exception {
    @Serial
    private static final long serialVersionUID = 756964120041563850L;
    private int status = -1;

    public RefreshException(String message, int status) {
        super(message);
        this.status = status;
    }

    public RefreshException(String message, Throwable cause, int status) {
        super(message, cause);
        this.status = status;
    }
}