package com.workflows.backend.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serial;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class ValidationException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = -7342171405219774216L;

    private final HttpStatus status;
    private final List<String> errors;
}