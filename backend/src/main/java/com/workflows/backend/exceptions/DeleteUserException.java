package com.workflows.backend.exceptions;

import lombok.Getter;

import java.io.Serial;

@Getter
public class DeleteUserException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = -315206376535117316L;

    private int status = -1;

    public DeleteUserException(String message, int status) {
        super(message);
        this.status = status;
    }

    public DeleteUserException(String message, Throwable cause, int status) {
        super(message, cause);
        this.status = status;
    }
}