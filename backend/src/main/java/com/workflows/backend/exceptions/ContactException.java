package com.workflows.backend.exceptions;

import java.io.Serial;

public class ContactException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -1839273210839019110L;

    public ContactException(String message){
        super(message);
    }

    public ContactException(String message, Throwable cause) {
        super(message, cause);
    }
}