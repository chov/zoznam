package com.workflows.backend.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Address implements Serializable {

    @Serial
    private static final long serialVersionUID = -6693970439482249980L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
    private Set<Contact> contacts = new HashSet<>();

    private String street;

    @Column(name = "house_num")
    private Integer houseNum;

    private String city;

    private String country;

    public Set<Contact> getContacts() {
        return contacts;
    }
}