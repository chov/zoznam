package com.workflows.backend.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Types;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Contact implements Serializable {

    @Serial
    private static final long serialVersionUID = 3513634228115989348L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    private String name;

    private String surname;

    private LocalDate birthdate;

    @Column(name = "phone_num")
    private String phoneNum;

    private String keycloak_id;

    @Lob
    @JdbcTypeCode(Types.BINARY)
    private byte[] image;

    @ManyToOne(optional = false)
    @JoinColumn(name = "address_id")
    private Address address;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "hobby", joinColumns = @JoinColumn(name = "contactId"))
    @Column(name = "name")
    private Set<String> hobbies;
}