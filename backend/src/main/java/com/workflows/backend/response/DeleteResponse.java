package com.workflows.backend.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class DeleteResponse {

    public static ResponseEntity<String> handleResponse() {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Content deleted");
    }
}
