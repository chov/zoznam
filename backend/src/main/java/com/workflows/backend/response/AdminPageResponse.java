package com.workflows.backend.response;

import com.workflows.backend.DTO.ContactAdminDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminPageResponse {

    private List<ContactAdminDTO> contacts;

    private int numOfFilteredContacts;
}
