package com.workflows.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.keycloak.representations.AccessTokenResponse;

@Getter
@Setter
@AllArgsConstructor
public class AccessTokenResp {

    private AccessTokenResponse accessToken;

    private boolean isAdmin;
}
