package com.workflows.backend.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class RegisterResponse {

    public static ResponseEntity<AccessTokenResp> handleResponse(AccessTokenResp response) {
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
}
