package com.workflows.backend.response;

import com.workflows.backend.DTO.ContactDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PageResponse {

    private List<ContactDTO> contacts;

    private int numOfFilteredContacts;
}
