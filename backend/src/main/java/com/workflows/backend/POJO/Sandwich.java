package com.workflows.backend.POJO;

public class Sandwich {

    private String bread;

    private String spread;

    private boolean vegetable;

    private boolean pepperoni;

    public String getBread() {
        return this.bread;
    }

    public String getSpread() {
        return this.spread;
    }

    public boolean havingVegetable() {
        return this.vegetable;
    }

    public boolean havingPepperoni() {
        return this.pepperoni;
    }

    private Sandwich(SandwichBuilder builder) {
        this.bread = builder.bread;
        this.spread = builder.spread;
        this.vegetable = builder.vegetable;
        this.pepperoni = builder.pepperoni;
    }

    //entity manager requires noArgsConstructor
    private Sandwich(){
    }

    public static class SandwichBuilder {

        private String bread;

        private String spread;

        private boolean vegetable;

        private boolean pepperoni;

        public SandwichBuilder() {

        }

        public SandwichBuilder setBread(String bread) {
            this.bread = bread;
            return this;
        }

        public SandwichBuilder setSpread(String spread){
            this.spread = spread;
            return this;
        }

        public SandwichBuilder setHavingVegetable(boolean vegetable) {
            this.vegetable = vegetable;
            return this;
        }

        public SandwichBuilder setHavingPepperoni(boolean pepperoni) {
            this.pepperoni = pepperoni;
            return this;
        }

        public Sandwich build() {
            return new Sandwich(this);
        }
    }
}
