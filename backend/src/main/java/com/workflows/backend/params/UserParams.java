package com.workflows.backend.params;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserParams extends FilterParams{

    private String userCriterion;

    private Boolean userExactCriterion;
}