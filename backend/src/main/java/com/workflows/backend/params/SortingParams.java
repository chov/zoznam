package com.workflows.backend.params;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SortingParams {

    private String sortBy;

    private String direction;
}