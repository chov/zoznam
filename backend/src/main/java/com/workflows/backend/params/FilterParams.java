package com.workflows.backend.params;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@RequiredArgsConstructor
public class FilterParams {

    private String nameCriterion;

    private Boolean nameExactCriterion;

    private String surnameCriterion;

    private Boolean surnameExactCriterion;

    private LocalDate birthdateCriterion;

    private String birthdateExactCriterion;

    private String phoneNumCriterion;

    private Boolean phoneNumExactCriterion;

    private String[] hobbies;

    private String streetCriterion;

    private Boolean streetExactCriterion;

    private String cityCriterion;

    private Boolean cityExactCriterion;

    private String countryCriterion;

    private Boolean countryExactCriterion;
}