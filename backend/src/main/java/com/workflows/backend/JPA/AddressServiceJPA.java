package com.workflows.backend.JPA;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.exceptions.AddressException;
import com.workflows.backend.mapper.AddressMapper;
import com.workflows.backend.service.AddressService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Qualifier("JPA")
@Service
public class AddressServiceJPA implements AddressService {

    @PersistenceContext
    private EntityManager entityManager;

    private final CriteriaBuilder criteriaBuilder;

    private final AddressMapper addressMapper;

    public AddressServiceJPA(EntityManager entityManager, AddressMapper addressMapper) {
        this.entityManager = entityManager;
        criteriaBuilder = entityManager.getCriteriaBuilder();
        this.addressMapper = addressMapper;
    }

    @Override
    public void addAddress(Address address) throws AddressException {
        if (address == null) {
            throw new AddressException("Address is null");
        }
        entityManager.persist(address);
    }

    @Override
    public AddressDTO getAddressByContactId(Long contactId) throws AddressException {
        if (contactId == null) {
            throw new AddressException("Contact ID is null!");
        }
        if (entityManager.find(Contact.class, contactId) == null) {
            throw new AddressException("Contact with this ID was not found!");
        }
        return addressMapper.entityToDto(entityManager.find(Contact.class, contactId).getAddress());
    }

    @Override
    public AddressDTO getAddressById(Long addressId) {
        if (addressId == null) {
            throw new AddressException("Address is null");
        }
        return addressMapper.entityToDto(entityManager.find(Address.class, addressId));
    }

    @Override
    public Optional<Address> getDuplicateAddress(Address address) {
        if (address == null) {
            return Optional.empty();
        }
        CriteriaQuery<Address> criteriaQuery = criteriaBuilder.createQuery(Address.class);
        Root<Address> root = criteriaQuery.from(Address.class);

        Predicate streetPredicate = criteriaBuilder.or(
                criteriaBuilder.and(
                        criteriaBuilder.isNull(root.get("street")),
                        criteriaBuilder.isNull(criteriaBuilder.literal(address.getStreet()))
                ),
                criteriaBuilder.equal(root.get("street"), address.getStreet()));

        Predicate cityPredicate = criteriaBuilder.or(
                criteriaBuilder.and(
                        criteriaBuilder.isNull(root.get("city")),
                        criteriaBuilder.isNull(criteriaBuilder.literal(address.getCity()))
                ),
                criteriaBuilder.equal(root.get("city"), address.getCity()));

        Predicate houseNumPredicate = criteriaBuilder.or(
                criteriaBuilder.and(
                        criteriaBuilder.isNull(root.get("houseNum")),
                        criteriaBuilder.isNull(criteriaBuilder.literal(address.getHouseNum()))
                ),
                criteriaBuilder.equal(root.get("houseNum"), address.getHouseNum()));

        Predicate countryPredicate = criteriaBuilder.or(
                criteriaBuilder.and(
                        criteriaBuilder.isNull(root.get("country")),
                        criteriaBuilder.isNull(criteriaBuilder.literal(address.getCountry()))
                ),
                criteriaBuilder.equal(root.get("country"), address.getCountry()));

        Predicate combinedPredicate = criteriaBuilder.and(
                streetPredicate,
                houseNumPredicate,
                cityPredicate,
                countryPredicate
        );

        criteriaQuery.select(root).where(combinedPredicate);
        List<Address> matchingAddresses = entityManager.createQuery(criteriaQuery).getResultList();
        if (!matchingAddresses.isEmpty()) {
            return Optional.of(matchingAddresses.get(0));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void deleteAddressWithNoContacts(Address address) {
        if (address.getContacts().size() == 0) {
            entityManager.remove(address);
        }
    }

    @Override
    public void deleteAddress(Long addressId) throws DataIntegrityViolationException, AddressException {
        if (addressId == null) {
            throw new AddressException("Address is null");
        }
        if (entityManager.find(Address.class, addressId) == null) {
            throw new AddressException("Address with this id was not found!");
        }
        entityManager.remove(entityManager.find(Address.class, addressId));
    }
}