package com.workflows.backend.JPA;

import com.workflows.backend.DTO.PersonDTO;
import com.workflows.backend.entity.Person;
import com.workflows.backend.mapper.PersonMapper;
import com.workflows.backend.service.PersonService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PersonServiceJPA implements PersonService {

    @PersistenceContext
    private EntityManager entityManager;

    private final PersonMapper personMapper;

    @Override
    public PersonDTO addPerson(PersonDTO personDTO) {
        Person person = personMapper.dtoToEntity(personDTO);
        entityManager.persist(person);
        return personMapper.entityToDto(person);
    }

    @Override
    public List<PersonDTO> getAllPersons() {
        return personMapper.entityListToDto(
                entityManager.createQuery("SELECT p FROM Person p").getResultList());
    }

    @Override
    public PersonDTO getPerson(Long personId) {
        return personMapper.entityToDto(entityManager.find(Person.class, personId));
    }

    @Override
    public PersonDTO updatePerson(PersonDTO personDto) {
        Person person = personMapper.dtoToEntity(personDto);
        entityManager.merge(person);
        return personMapper.entityToDto(person);
    }

    @Override
    public void deletePerson(Long personId) {
        entityManager.remove(entityManager.find(Person.class, personId));
    }
}