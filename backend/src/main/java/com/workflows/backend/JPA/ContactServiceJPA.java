package com.workflows.backend.JPA;

import com.workflows.backend.DTO.ContactAdminDTO;
import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.exceptions.ContactException;
import com.workflows.backend.mapper.ContactMapper;
import com.workflows.backend.params.FilterParams;
import com.workflows.backend.params.PagingParams;
import com.workflows.backend.params.SortingParams;
import com.workflows.backend.params.UserParams;
import com.workflows.backend.repository.ContactSpecification;
import com.workflows.backend.response.AdminPageResponse;
import com.workflows.backend.response.DeleteResponse;
import com.workflows.backend.response.PageResponse;
import com.workflows.backend.service.AddressService;
import com.workflows.backend.service.ContactService;
import com.workflows.backend.service.UserService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Qualifier("JPA")
@Service
public class ContactServiceJPA implements ContactService {

    @PersistenceContext
    private EntityManager entityManager;

    private final ContactMapper contactMapper;

    private final AddressService addressService;

    private final UserService userService;

    private final CriteriaBuilder criteriaBuilder;

    public ContactServiceJPA(EntityManager entityManager, ContactMapper contactMapper, @Qualifier("JPA") AddressService addressService, UserService userService) {
        this.entityManager = entityManager;
        this.contactMapper = contactMapper;
        this.addressService = addressService;
        this.userService = userService;
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public ContactDTO addContact(ContactDTO contactDto) throws ContactException {
        if (contactDto == null) {
            throw new ContactException("ContactDto is null!");
        }
        Contact contact = contactMapper.dtoToEntity(contactDto);
        Optional<Address> dupAddress = addressService.getDuplicateAddress(contact.getAddress());
        dupAddress.ifPresentOrElse(
                contact::setAddress,
                () -> addressService.addAddress(contact.getAddress()));
        entityManager.persist(contact);
        return contactMapper.entityToDto(contact);
    }

    @Override
    public void deleteContactsOfUser(String keycloakId) {
        if (keycloakId == null) {
            throw new ContactException("KeycloakId is null!");
        }
        CriteriaDelete<Contact> deleteQuery = criteriaBuilder.createCriteriaDelete(Contact.class);
        Root<Contact> root = deleteQuery.from(Contact.class);
        deleteQuery.where(criteriaBuilder.equal(root.get("keycloak_id"), keycloakId));
        entityManager.createQuery(deleteQuery).executeUpdate();
    }

    @Override
    public int getNumOfAllContacts() throws ContactException {
        String queryString = "SELECT COUNT(c) FROM Contact c";
        TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
        return query.getSingleResult().intValue();
    }

    @Override
    public ResponseEntity<String> deleteContact(Long contactId) throws ContactException {
        if (contactId == null) {
            throw new ContactException("ContactId is null!");
        }
        if (entityManager.find(Contact.class, contactId) == null) {
            throw new ContactException("Contact with this id was not found!");
        }

        Address previousAddress = entityManager.find(Contact.class, contactId).getAddress();
        try {
            entityManager.remove(entityManager.find(Contact.class, contactId));
        } catch (IllegalArgumentException exception) {
            throw new ContactException(exception.getMessage());
        }
        entityManager.flush();
        addressService.deleteAddressWithNoContacts(previousAddress);
        return DeleteResponse.handleResponse();
    }

    @Override
    public ContactDTO getContactById(Long contactId) throws ContactException {
        if (contactId == null) {
            throw new ContactException("ContactId is null!");
        }
        Contact contact = entityManager.find(Contact.class, contactId);
        if (contact == null) {
            throw new ContactException("Contact with this id was not found!");
        }
        return contactMapper.entityToDto(contact);
    }

    @Override
    public ContactDTO updateContact(ContactDTO contactDto) throws ContactException {
        Contact contact = contactMapper.dtoToEntity(contactDto);
        Address previousAddress = entityManager.find(Contact.class, contact.getId()).getAddress();
        Optional<Address> dupAddress = addressService.getDuplicateAddress(contact.getAddress());
        dupAddress.ifPresentOrElse(
                contact::setAddress,
                () -> addressService.addAddress(contact.getAddress()));
        entityManager.merge(contact);
        entityManager.flush();
        addressService.deleteAddressWithNoContacts(previousAddress);

        return contactMapper.entityToDto(contact);
    }

    @Override
    public PageResponse getPageOfContacts(PagingParams pagingParams,
                                          SortingParams sortingParams,
                                          FilterParams filterParams) throws ContactException {

        CriteriaQuery<Contact> criteriaQuery = criteriaBuilder.createQuery(Contact.class);
        Root<Contact> root = criteriaQuery.from(Contact.class);

        //gets Specification(list of predicates) for filtering
        ContactSpecification spec = new ContactSpecification(filterParams, List.of(userService.getUserId()));

        Pair<TypedQuery<Contact>, Integer> query = defineQuery(criteriaQuery, spec, root, sortingParams, pagingParams);

        return new PageResponse(listToDto(query.getFirst().getResultList()), query.getSecond());
    }

    @Override
    public AdminPageResponse getPageOfAllContacts(PagingParams pagingParams,
                                                  SortingParams sortingParams,
                                                  UserParams userParams) throws ContactException {
        CriteriaQuery<Contact> criteriaQuery = criteriaBuilder.createQuery(Contact.class);
        Root<Contact> root = criteriaQuery.from(Contact.class);

        List<String> usersId = userService.getUserId(userParams.getUserCriterion(), userParams.getUserExactCriterion());

        //gets Specification(list of predicates) for filtering
        ContactSpecification spec = new ContactSpecification(userParams, usersId);

        Pair<TypedQuery<Contact>, Integer> query = defineQuery(criteriaQuery, spec, root, sortingParams, pagingParams);

        return new AdminPageResponse(listToAdminDto(query.getFirst().getResultList()), query.getSecond());
    }

    private Pair<TypedQuery<Contact>, Integer> defineQuery(
            CriteriaQuery<Contact> criteriaQuery,
            ContactSpecification spec,
            Root<Contact> root,
            SortingParams sortingParams,
            PagingParams pagingParams) {
        criteriaQuery.select(root).where(spec.toPredicate(root, criteriaQuery, criteriaBuilder));

        //if sorting parameters are present, they're set up
        criteriaQuery = setSorting(sortingParams, criteriaQuery, root);

        //creates query with filtering and sorting set up
        TypedQuery<Contact> query = entityManager.createQuery(criteriaQuery);
        //gets number of all filtered contacts before paging is set
        int numOfFilteredContacts = (int) query.getResultStream().count();

        query.setFirstResult(pagingParams.getOffset() * pagingParams.getLimit());
        query.setMaxResults(pagingParams.getLimit());

        return Pair.of(query, numOfFilteredContacts);
    }

    private CriteriaQuery<Contact> setSorting(SortingParams sortingParams, CriteriaQuery<Contact> criteriaQuery, Root<Contact> root) {
        if (sortingParams == null || sortingParams.getSortBy() == null) {
            return criteriaQuery;
        }
        if (sortingParams.getSortBy().equalsIgnoreCase("user")) {
            return criteriaQuery;
        }
        if (sortingParams.getDirection().equalsIgnoreCase("asc")) {
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortingParams.getSortBy())));
        } else if (sortingParams.getDirection().equals("desc")) {
            criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sortingParams.getSortBy())));
        }
        return criteriaQuery;
    }

    public List<ContactDTO> listToDto(List<Contact> contacts) {
        return contacts.stream()
                .map(contactMapper::entityToDto)
                .collect(Collectors.toList());
    }

    public List<ContactAdminDTO> listToAdminDto(List<Contact> contacts) {
        return contacts.stream()
                .map(contactMapper::entityToAdminDto)
                .collect(Collectors.toList());
    }
}