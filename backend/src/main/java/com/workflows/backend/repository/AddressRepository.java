package com.workflows.backend.repository;

import com.workflows.backend.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

    @Modifying
    @Query("DELETE FROM Address a WHERE SIZE(a.contacts) = 0 AND a.id = :addressId")
    void deleteAddressWithNoContacts(Long addressId);

    @Query("SELECT SIZE(a.contacts) FROM Address a WHERE a.id = :addressId")
    int select(Long addressId);

    @Query("SELECT a FROM Address a JOIN a.contacts c WHERE c.id = :contactId")
    Address findByContactIdInContacts(Long contactId);
}