package com.workflows.backend.repository;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.entity.Address;
import com.workflows.backend.exceptions.AddressException;
import com.workflows.backend.mapper.AddressMapper;
import com.workflows.backend.service.AddressService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Qualifier("repository")
@Service
public class AddressServiceRepository implements AddressService {

    private final AddressRepository addressRepository;

    private final AddressMapper addressMapper;

    public AddressServiceRepository(AddressRepository addressRepository, AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    public void addAddress(Address address) {
        if (address == null) {
            throw new AddressException("Address is null!");
        }
        addressRepository.save(address);
    }

    public AddressDTO getAddressByContactId(Long contactId) {
        if (contactId == null) {
            throw new AddressException("Address is null");
        }
        return addressMapper.entityToDto(addressRepository.findByContactIdInContacts(contactId));
    }

    public Optional<Address> getDuplicateAddress(Address address) {
        if (address == null) {
            return Optional.empty();
        }
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("id")
                .withIncludeNullValues();
        Example<Address> example = Example.of(address, matcher);
        return addressRepository.findOne(example);
    }

    public void deleteAddressWithNoContacts(Address address) {
        addressRepository.deleteAddressWithNoContacts(address.getId());
    }

    @Override
    public void deleteAddress(Long addressId) {
        addressRepository.deleteById(addressId);
    }

    @Override
    public AddressDTO getAddressById(Long addressId) {
        if (addressId == null) {
            throw new AddressException("Address is null");
        }
        return addressMapper.entityToDto(addressRepository.findById(addressId).orElse(null));
    }
}