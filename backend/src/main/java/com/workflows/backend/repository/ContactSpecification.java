package com.workflows.backend.repository;

import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.exceptions.ContactException;
import com.workflows.backend.params.FilterParams;
import jakarta.persistence.criteria.*;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
public class ContactSpecification implements Specification<Contact> {

    @Serial
    private static final long serialVersionUID = 2053156595813618268L;

    private FilterParams filterParams;

    private List<String> userId;

    @Override
    public Predicate toPredicate
            (Root<Contact> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        ArrayList<Predicate> predicatesList = new ArrayList<>();
        Join<Contact, Address> addressJoin = root.join("address");

        if (!userId.isEmpty()) {
            predicatesList.add(root.get("keycloak_id").in(userId));
        }

        if (filterParams.getNameCriterion() != null) {
            if (filterParams.getNameExactCriterion() == null) {
                throw new ContactException("Missing parameter(nameExactCriterion), parameter must be specified for request to be valid!");
            }
            if (filterParams.getNameExactCriterion()) {
                predicatesList.add(criteriaBuilder.equal(root.get("name"), filterParams.getNameCriterion()));
            } else {
                predicatesList.add(criteriaBuilder.or
                        (criteriaBuilder.like(criteriaBuilder.upper(root.get("name")), filterParams.getNameCriterion().toUpperCase() + "%"),
                                criteriaBuilder.isNull(criteriaBuilder.literal(filterParams.getNameCriterion()))));
            }
        }
        if (filterParams.getSurnameCriterion() != null) {
            if (filterParams.getStreetExactCriterion() == null) {
                throw new ContactException("Missing parameter(surnameExactCriterion), parameter must be specified for request to be valid!");
            }
            if (filterParams.getSurnameExactCriterion()) {
                predicatesList.add(criteriaBuilder.equal(root.get("surname"), filterParams.getSurnameCriterion()));
            } else {
                predicatesList.add(criteriaBuilder.or
                        (criteriaBuilder.like(criteriaBuilder.upper(root.get("surname")), filterParams.getSurnameCriterion().toUpperCase() + "%"),
                                criteriaBuilder.isNull(criteriaBuilder.literal(filterParams.getSurnameCriterion()))));
            }
        }
        if (filterParams.getPhoneNumCriterion() != null) {
            if (filterParams.getPhoneNumExactCriterion() == null) {
                throw new ContactException("Missing parameter(phoneNumExactCriterion), parameter must be specified for request to be valid!");
            }
            if (filterParams.getPhoneNumExactCriterion()) {
                predicatesList.add(criteriaBuilder.equal(root.get("phoneNum"), filterParams.getPhoneNumCriterion()));
            } else {
                predicatesList.add(criteriaBuilder.or
                        (criteriaBuilder.like(criteriaBuilder.upper(root.get("phoneNum")), filterParams.getPhoneNumCriterion().toUpperCase() + "%"),
                                criteriaBuilder.isNull(criteriaBuilder.literal(filterParams.getPhoneNumCriterion()))));
            }
        }
        if (filterParams.getBirthdateCriterion() != null) {
            if (filterParams.getBirthdateExactCriterion() == null) {
                throw new ContactException("Missing parameter(birthdateExactCriterion), parameter must be specified for request to be valid!");
            }
            switch (filterParams.getBirthdateExactCriterion()) {
                case "exact" ->
                        predicatesList.add(criteriaBuilder.equal(root.get("birthdate"), filterParams.getBirthdateCriterion()));
                case "later" ->
                        predicatesList.add(criteriaBuilder.greaterThan(root.get("birthdate"), filterParams.getBirthdateCriterion()));
                case "before" ->
                        predicatesList.add(criteriaBuilder.lessThan(root.get("birthdate"), filterParams.getBirthdateCriterion()));
                default -> throw new ContactException("Birthdate exact criterion did not match any of the cases!");
            }
        }
        if (filterParams.getHobbies() != null) {
            Predicate[] hobbyPredicates = Arrays.stream(filterParams.getHobbies())
                    .map(hobby -> criteriaBuilder.isMember(hobby, root.get("hobbies")))
                    .toArray(Predicate[]::new);
            predicatesList.add(criteriaBuilder.or(hobbyPredicates));
        }
        if (filterParams.getStreetCriterion() != null) {
            if (filterParams.getStreetExactCriterion() == null) {
                throw new ContactException("Missing parameter(streetExactCriterion), parameter must be specified for request to be valid!");
            }
            if (filterParams.getStreetExactCriterion()) {
                predicatesList.add(criteriaBuilder.equal(addressJoin.get("street"), filterParams.getStreetCriterion()));
            } else {
                predicatesList.add(criteriaBuilder.or
                        (criteriaBuilder.like(criteriaBuilder.upper(addressJoin.get("street")), filterParams.getStreetCriterion().toUpperCase() + "%"),
                                criteriaBuilder.isNull(criteriaBuilder.literal(filterParams.getStreetCriterion()))));
            }
        }
        if (filterParams.getCityCriterion() != null) {
            if (filterParams.getCityExactCriterion() == null) {
                throw new ContactException("Missing parameter(cityExactCriterion), parameter must be specified for request to be valid!");
            }
            if (filterParams.getCityExactCriterion()) {
                predicatesList.add(criteriaBuilder.equal(addressJoin.get("city"), filterParams.getCityCriterion()));
            } else {
                predicatesList.add(criteriaBuilder.or
                        (criteriaBuilder.like(criteriaBuilder.upper(addressJoin.get("city")), filterParams.getCityCriterion().toUpperCase() + "%"),
                                criteriaBuilder.isNull(criteriaBuilder.literal(filterParams.getCityCriterion()))));
            }
        }
        if (filterParams.getCountryCriterion() != null) {
            if (filterParams.getCountryExactCriterion() == null) {
                throw new ContactException("Missing parameter(countryExactCriterion), parameter must be specified for request to be valid!");
            }
            if (filterParams.getCountryExactCriterion()) {
                predicatesList.add(criteriaBuilder.equal(addressJoin.get("country"), filterParams.getCountryCriterion()));
            } else {
                predicatesList.add(criteriaBuilder.or
                        (criteriaBuilder.like(criteriaBuilder.upper(addressJoin.get("country")), filterParams.getCountryCriterion().toUpperCase() + "%"),
                                criteriaBuilder.isNull(criteriaBuilder.literal(filterParams.getCountryCriterion()))));
            }
        }

        Predicate[] predicates = new Predicate[predicatesList.size()];
        predicatesList.toArray(predicates);
        return criteriaBuilder.and(predicates);
    }
}