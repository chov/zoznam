package com.workflows.backend.repository;

import com.workflows.backend.DTO.ContactAdminDTO;
import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.entity.Address;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.exceptions.ContactException;
import com.workflows.backend.mapper.ContactMapper;
import com.workflows.backend.params.FilterParams;
import com.workflows.backend.params.PagingParams;
import com.workflows.backend.params.SortingParams;
import com.workflows.backend.params.UserParams;
import com.workflows.backend.response.AdminPageResponse;
import com.workflows.backend.response.DeleteResponse;
import com.workflows.backend.response.PageResponse;
import com.workflows.backend.service.AddressService;
import com.workflows.backend.service.ContactService;
import com.workflows.backend.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Qualifier("repository")
@Service
public class ContactServiceRepository implements ContactService {

    private final ContactRepository contactRepository;

    private final AddressService addressService;

    private final ContactMapper contactMapper;

    private final UserService userService;

    public ContactServiceRepository(ContactRepository contactRepository, ContactMapper contactMapper, @Qualifier("repository") AddressService addressService, UserService userService) {
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
        this.addressService = addressService;
        this.userService = userService;
    }

    @Override
    public ContactDTO addContact(ContactDTO contactDTO) {
        if (contactDTO == null) {
            throw new ContactException("Contact is null!");
        }
        Contact contact = contactMapper.dtoToEntity(contactDTO);

        Optional<Address> dupAddress = addressService.getDuplicateAddress(contact.getAddress());
        dupAddress.ifPresentOrElse(
                contact::setAddress,
                () -> addressService.addAddress(contact.getAddress()));

        return contactMapper.entityToDto(contactRepository.save(contact));
    }

    @Override
    public void deleteContactsOfUser(String keycloakId) {
        if (keycloakId == null) {
            throw new ContactException("Keycloak ID is null!");
        }
        contactRepository.deleteByKeycloak_id(keycloakId);
    }

    @Override
    public ContactDTO updateContact(ContactDTO contactDto) {
        Contact contact = contactMapper.dtoToEntity(contactDto);
        //1, get previous address
        Address previousAddress = contact.getAddress();
        //2, check if new address is already in database
        Optional<Address> dupAddress = addressService.getDuplicateAddress(contact.getAddress());
        dupAddress.ifPresentOrElse(
                contact::setAddress,
                () -> addressService.addAddress(contact.getAddress()));

        //4, update contact
        contactRepository.save(contact);
        //5, if previous address isn't attached to any contact it's deleted from DB
        addressService.deleteAddressWithNoContacts(previousAddress);

        return contactMapper.entityToDto(contact);
    }

    @Override
    public ResponseEntity<String> deleteContact(Long contactId) {
        if (contactId == null) {
            throw new ContactException("Contact ID is null!");
        }
        if (contactRepository.findById(contactId).isEmpty()) {
            throw new ContactException("Contact with this id was not found!");
        }
        Address previousAddress = contactRepository.findById(contactId).get().getAddress();
        contactRepository.deleteById(contactId);
        addressService.deleteAddressWithNoContacts(previousAddress);
        return DeleteResponse.handleResponse();
    }

    @Override
    public PageResponse getPageOfContacts(PagingParams pagingParams, SortingParams sortingParams, FilterParams filterParams) {
        PageRequest page = PageRequest.of(pagingParams.getOffset(), pagingParams.getLimit());

        page = setSorting(page, sortingParams);

        ContactSpecification spec = new ContactSpecification(filterParams, List.of(userService.getUserId()));

        int numOfFilteredContacts = contactRepository.findAll(spec).size();

        List<Contact> contacts = contactRepository.findAll(spec, page).getContent();
        return new PageResponse(listToDto(contacts), numOfFilteredContacts);
    }

    @Override
    public AdminPageResponse getPageOfAllContacts(PagingParams pagingParams, SortingParams sortingParams, UserParams userParams) {
        PageRequest page = PageRequest.of(pagingParams.getOffset(), pagingParams.getLimit());
        page = setSorting(page, sortingParams);

        List<String> usersId = userService.getUserId(userParams.getUserCriterion(), userParams.getUserExactCriterion());
        ContactSpecification spec = new ContactSpecification(userParams, usersId);
        int numOfFilteredContacts = contactRepository.findAll(spec).size();
        List<Contact> contacts = contactRepository.findAll(spec, page).getContent();
        return new AdminPageResponse(listToAdminDto(contacts), numOfFilteredContacts);
    }

    public List<ContactDTO> listToDto(List<Contact> contacts) {
        return contacts.stream()
                .map(contactMapper::entityToDto)
                .collect(Collectors.toList());
    }

    private PageRequest setSorting(PageRequest page, SortingParams sortingParams) {
        if (sortingParams != null && sortingParams.getSortBy() != null && !sortingParams.getSortBy().equalsIgnoreCase("user")) {
            if (sortingParams.getDirection().equals("asc")) {
                page = page.withSort(Sort.by(sortingParams.getSortBy()).ascending());
            } else if (sortingParams.getDirection().equals("desc")) {
                page = page.withSort(Sort.by(sortingParams.getSortBy()).descending());
            }
        }
        return page;
    }

    @Override
    public int getNumOfAllContacts() {
        return (int) contactRepository.count();
    }

    @Override
    public ContactDTO getContactById(Long contactId) {
        if (contactId == null) {
            throw new ContactException("Contact ID is null!");
        }
        Contact contact = contactRepository.findById(contactId).orElse(null);
        if (contact == null) {
            throw new ContactException("Contact with this id was not found!");
        }
        return contactMapper.entityToDto(contact);
    }

    public List<ContactAdminDTO> listToAdminDto(List<Contact> contacts) {
        return contacts.stream()
                .map(contactMapper::entityToAdminDto)
                .collect(Collectors.toList());
    }
}