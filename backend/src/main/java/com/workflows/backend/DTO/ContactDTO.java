package com.workflows.backend.DTO;

import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactDTO {

    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String surname;

    @PositiveOrZero
    private int age;

    @NotNull
    @Past
    private LocalDate birthdate;

    @Pattern(regexp = "^(\\+\\d{3}|0)\\d{9}$")
    private String phoneNum;

    private String image;

    private Set<String> hobbies;

    @NotNull
    @Valid
    private AddressDTO address;
}
