package com.workflows.backend.DTO;

import com.workflows.backend.POJO.Backpack;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PersonDTO {

    private Long id;

    private String name;

    private String surname;

    @PositiveOrZero
    private int age;

    private Backpack backpack;

}
