package com.workflows.backend.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactAdminDTO extends ContactDTO {

    private String user;

    public ContactAdminDTO(Long id, String user, String name, String surname, int age, LocalDate birthdate, String phoneNum, String image, Set<String> hobbies, AddressDTO address) {
        super(id, name, surname, age, birthdate, phoneNum, image, hobbies, address);
        this.user = user;
    }
}