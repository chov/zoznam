package com.workflows.backend.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@RequiredArgsConstructor
@Profile({"default"})
@Getter
@Configuration
public class KeycloakConfig {

    public static Keycloak keycloak = null;

    @Value("${jwt.auth.converter.resource-uri}")
    private String serverUrl;

    @Value("${jwt.auth.converter.resource-realm}")
    private String realm;

    @Value("${jwt.auth.converter.resource-id}")
    private String clientId;

    @Value("${jwt.auth.converter.resource-secret}")
    private String clientSecret;

    private final static String userName = "user1";

    private final static String password = "user1";

    @Bean
    public Keycloak getInstance() {
        if (keycloak == null) {
            keycloak = Keycloak.getInstance(serverUrl, realm,
                    userName, password, clientId, clientSecret);
        }
        return keycloak;
    }
}