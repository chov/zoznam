package com.workflows.backend.mapper;

import com.workflows.backend.DTO.ContactAdminDTO;
import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.entity.Contact;
import com.workflows.backend.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@AllArgsConstructor
@Component
public class ContactMapperImpl implements ContactMapper {

    private final AddressMapper addressMapper;

    private final UserService userService;

    @Override
    public ContactDTO entityToDto(Contact contact) {
        if (contact == null) {
            return null;
        }

        ContactDTO contactDTO = new ContactDTO();

        contactDTO.setId(contact.getId());
        contactDTO.setName(contact.getName());
        contactDTO.setSurname(contact.getSurname());
        contactDTO.setBirthdate(contact.getBirthdate());
        contactDTO.setPhoneNum(contact.getPhoneNum());
        if (contact.getImage() != null) {
            contactDTO.setImage(new String(Base64.getDecoder().decode(contact.getImage())));
        }
        contactDTO.setHobbies(contact.getHobbies());
        contactDTO.setAddress(addressMapper.entityToDto(contact.getAddress()));

        return contactDTO;
    }

    @Override
    public ContactAdminDTO entityToAdminDto(Contact contact) {
        if (contact == null) {
            return null;
        }

        ContactAdminDTO contactAdminDTO = new ContactAdminDTO();

        contactAdminDTO.setId(contact.getId());
        contactAdminDTO.setName(contact.getName());
        contactAdminDTO.setSurname(contact.getSurname());
        contactAdminDTO.setBirthdate(contact.getBirthdate());
        contactAdminDTO.setPhoneNum(contact.getPhoneNum());
        if (contact.getImage() != null) {
            contactAdminDTO.setImage(new String(Base64.getDecoder().decode(contact.getImage())));
        }
        contactAdminDTO.setHobbies(contact.getHobbies());
        contactAdminDTO.setAddress(addressMapper.entityToDto(contact.getAddress()));
        contactAdminDTO.setUser(userService.getUserName(contact.getKeycloak_id()));


        return contactAdminDTO;
    }

    @Override
    public Contact dtoToEntity(ContactDTO contactDTO) {
        if (contactDTO == null) {
            return null;
        }

        Contact contact = new Contact();

        contact.setId(contactDTO.getId());
        contact.setName(contactDTO.getName());
        contact.setSurname(contactDTO.getSurname());
        contact.setBirthdate(contactDTO.getBirthdate());
        contact.setPhoneNum(contactDTO.getPhoneNum());
        if (contactDTO.getImage() != null) {
            contact.setImage(Base64.getEncoder().encode(contactDTO.getImage().getBytes(StandardCharsets.UTF_8)));
        }
        contact.setHobbies(contactDTO.getHobbies());
        contact.setAddress(addressMapper.dtoToEntity(contactDTO.getAddress()));
        Jwt keycloakToken = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        contact.setKeycloak_id(keycloakToken.getClaimAsString("sub"));

        return contact;
    }
}