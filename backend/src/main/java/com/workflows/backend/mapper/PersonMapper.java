package com.workflows.backend.mapper;

import com.workflows.backend.DTO.PersonDTO;
import com.workflows.backend.entity.Person;

import java.util.List;

public interface PersonMapper {

    PersonDTO entityToDto(Person person);

    Person dtoToEntity(PersonDTO personDTO);

    List<PersonDTO> entityListToDto(List<Person> personList);
}
