package com.workflows.backend.mapper;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.entity.Address;
import org.springframework.stereotype.Component;

@Component
public class AddressMapperImpl implements AddressMapper {

    @Override
    public AddressDTO entityToDto(Address address) {
        if ( address == null ) {
            return null;
        }

        AddressDTO addressDTO = new AddressDTO();

        addressDTO.setId( address.getId() );
        addressDTO.setStreet( address.getStreet() );
        addressDTO.setHouseNum( address.getHouseNum() );
        addressDTO.setCity( address.getCity() );
        addressDTO.setCountry( address.getCountry() );

        return addressDTO;
    }

    @Override
    public Address dtoToEntity(AddressDTO addressDTO) {
        if ( addressDTO == null ) {
            return null;
        }

        Address address = new Address();

        address.setId( addressDTO.getId() );
        address.setStreet( addressDTO.getStreet() );
        address.setHouseNum( addressDTO.getHouseNum() );
        address.setCity( addressDTO.getCity() );
        address.setCountry( addressDTO.getCountry() );

        return address;
    }
}