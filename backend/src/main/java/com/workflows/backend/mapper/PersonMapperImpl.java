package com.workflows.backend.mapper;

import com.workflows.backend.DTO.PersonDTO;
import com.workflows.backend.entity.Person;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PersonMapperImpl implements PersonMapper {

    @Override
    public PersonDTO entityToDto(Person person) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setId(person.getId());
        personDTO.setName(person.getName());
        personDTO.setSurname(person.getSurname());
        personDTO.setAge(person.getAge());
        personDTO.setBackpack(person.getBackpack());
        return personDTO;
    }

    @Override
    public Person dtoToEntity(PersonDTO personDTO) {
        Person person = new Person();
        person.setId(personDTO.getId());
        person.setName(personDTO.getName());
        person.setSurname(personDTO.getSurname());
        person.setAge(personDTO.getAge());
        person.setBackpack(personDTO.getBackpack());
        return person;
    }

    @Override
    public List<PersonDTO> entityListToDto(List<Person> personList) {
        return personList.stream().map(this::entityToDto).collect(Collectors.toList());
    }
}
