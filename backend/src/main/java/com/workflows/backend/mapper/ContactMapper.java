package com.workflows.backend.mapper;


import com.workflows.backend.DTO.ContactAdminDTO;
import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.entity.Contact;

public interface ContactMapper {
    ContactDTO entityToDto(Contact contact);
    Contact dtoToEntity(ContactDTO contactDTO);
    ContactAdminDTO entityToAdminDto(Contact contact);
}