package com.workflows.backend.mapper;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.entity.Address;

public interface AddressMapper {

    AddressDTO entityToDto(Address address);

    Address dtoToEntity(AddressDTO addressDTO);
}