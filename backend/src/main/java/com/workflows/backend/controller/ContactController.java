package com.workflows.backend.controller;

import com.workflows.backend.DTO.ContactDTO;
import com.workflows.backend.params.FilterParams;
import com.workflows.backend.params.PagingParams;
import com.workflows.backend.params.SortingParams;
import com.workflows.backend.params.UserParams;
import com.workflows.backend.response.AdminPageResponse;
import com.workflows.backend.response.PageResponse;
import com.workflows.backend.service.ContactService;
import jakarta.validation.Valid;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;


@RequestMapping("/contact")
@Transactional
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@RestController
public class ContactController {

    private final ContactService contactService;

    public ContactController(@Qualifier("JPA") ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping
    public PageResponse getPageOfContacts(@ModelAttribute PagingParams pagingParams,
                                          @ModelAttribute SortingParams sortingParams,
                                          @ModelAttribute FilterParams filterParams) {
        return contactService.getPageOfContacts(pagingParams, sortingParams, filterParams);
    }

    @PreAuthorize("hasRole('admin')")
    @GetMapping("/all-contacts")
    public AdminPageResponse getPageOfAllContacts(@ModelAttribute PagingParams pagingParams,
                                                  @ModelAttribute SortingParams sortingParams,
                                                  @ModelAttribute UserParams userparams) throws ConstraintViolationException {
        return contactService.getPageOfAllContacts(pagingParams, sortingParams, userparams);
    }

    @PatchMapping
    public ContactDTO updateContact(@Valid @RequestBody ContactDTO contactDto) {
        return contactService.updateContact(contactDto);
    }

    @PostMapping
    public ContactDTO addContact(@Valid @RequestBody ContactDTO contact) {
        return contactService.addContact(contact);
    }

    @GetMapping("{contactId}")
    public ContactDTO getContactById(@PathVariable("contactId") Long contactId) {
        return contactService.getContactById(contactId);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteContact(@RequestParam Long contactId) {
        return contactService.deleteContact(contactId);
    }

    @GetMapping("/numOfContacts")
    public int getNumOfContacts() {
        return contactService.getNumOfAllContacts();
    }
}