package com.workflows.backend.controller;

import com.workflows.backend.DTO.UserDTO;
import com.workflows.backend.exceptions.DeleteUserException;
import com.workflows.backend.exceptions.RefreshException;
import com.workflows.backend.exceptions.RegistrationException;
import com.workflows.backend.response.AccessTokenResp;
import com.workflows.backend.service.ContactService;
import com.workflows.backend.service.UserServiceImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

@RequestMapping("/user")
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@Transactional
@RestController
public class UserController {

    private final UserServiceImpl userService;

    private final ContactService contactService;

    public UserController(UserServiceImpl userService, @Qualifier("repository") ContactService contactService) {
        this.userService = userService;
        this.contactService = contactService;
    }

    @GetMapping("/refresh")
    public AccessTokenResp refresh(@RequestHeader String refreshToken) throws RefreshException {
        return userService.refreshAccessToken(refreshToken);
    }

    @GetMapping
    public AccessTokenResp login(@ModelAttribute UserDTO userDTO) {
        return userService.login(userDTO);
    }

    @PostMapping
    public ResponseEntity<AccessTokenResp> registerUser(@Valid @RequestBody UserDTO userDTO) throws RegistrationException {
        return userService.registerUser(userDTO);
    }

    @DeleteMapping
    public void deleteUser() throws DeleteUserException {
        String userId = userService.getUserId();
        userService.deleteUser(userId);
        contactService.deleteContactsOfUser(userId);
    }

    @DeleteMapping("/{user}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<String> deleteUser(@PathVariable String user) throws DeleteUserException {
        String userId = userService.getUserId(user, true).get(0);
        ResponseEntity<String> response = userService.deleteUser(userId);
        contactService.deleteContactsOfUser(userId);
        return response;
    }

    @GetMapping("/is_admin")
    public boolean isAdmin() {
        return userService.getIsAdmin();
    }
}
