package com.workflows.backend.controller;

import com.workflows.backend.DTO.PersonDTO;
import com.workflows.backend.service.PersonService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

@RequiredArgsConstructor
@Transactional
@RequestMapping("/person")
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@RestController
public class PersonController {

    private final PersonService personService;

    @GetMapping
    public List<PersonDTO> getAllPersons() {
        return personService.getAllPersons();
    }

    @GetMapping("{personId}")
    public PersonDTO getPerson(@PathVariable("personId") Long personId) {
        return personService.getPerson(personId);
    }

    @PatchMapping
    public PersonDTO updatePerson(@RequestBody PersonDTO personDTO) {
        return personService.updatePerson(personDTO);
    }

    @PostMapping
    public PersonDTO addPerson(@RequestBody @Valid PersonDTO person) {
        return personService.addPerson(person);
    }

    @DeleteMapping
    public void deletePerson(@RequestParam Long personId) {
        personService.deletePerson(personId);
    }
}