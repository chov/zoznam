package com.workflows.backend.controller;

import com.workflows.backend.DTO.AddressDTO;
import com.workflows.backend.service.AddressService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

@RequestMapping("/address")
@Scope(WebApplicationContext.SCOPE_APPLICATION)
@RestController
public class AddressController {

    private final AddressService addressService;

    public AddressController(@Qualifier("JPA") AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping("/by_contact_id/{contactId}")
    public AddressDTO getAddressByContactId(@PathVariable("contactId") Long contactId) {
        return addressService.getAddressByContactId(contactId);
    }

    @DeleteMapping
    public void deleteAddress(@RequestParam("addressId") Long addressId) {
        addressService.deleteAddress(addressId);
    }

    @GetMapping
    public AddressDTO getAddress(@RequestParam("addressId") Long addressId) {
        return addressService.getAddressById(addressId);
    }
}