docker run -d \
  --name psql-db \
  --restart always \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_PASSWORD=root \
  -e POSTGRES_DB=zoznam \
  -p 5432:5432 \
  postgres:latest

# Wait for PostgreSQL to start up (you can customize the sleep time as needed)
sleep 10

# Start the backend container
docker build -t backend-app .
docker run -d \
  --name backend.jar \
  -p 8080:8080 \
  -e SPRING_DATASOURCE_URL=jdbc:postgresql://psql-db:5432/zoznam \
  -e SPRING_DATASOURCE_USERNAME=postgres \
  -e SPRING_DATASOURCE_PASSWORD=root \
  -e SPRING_JPA_HIBERNATE_DDL_AUTO=update \
  -e POSTGRES_PASSWORD=root \
  --depends-on psql-db \
  backend-app