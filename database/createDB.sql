create sequence kontakt_seq
    increment by 50;

alter sequence kontakt_seq owner to postgres;

create sequence adresa_seq
    increment by 50;

alter sequence adresa_seq owner to postgres;

create domain telcislo as varchar(15)
    constraint telcislo_check check ((VALUE)::text ~ '^(\+\d{3}|0)\d{9}$'::text);

alter domain telcislo owner to postgres;

create table adresa
(
    ulica      varchar(255),
    cislo_domu integer
        constraint positive_cislo_domu
            check (cislo_domu >= 0),
    mesto      varchar(255),
    krajina    varchar(255),
    adresa_id  bigint not null
        primary key
);

alter table adresa
    owner to postgres;

create table kontakt
(
    kontakt_id bigint       not null
        primary key,
    meno       varchar(255) not null,
    priezvisko varchar(255) not null,
    birthdate  timestamp(6) not null,
    telcislo   telcislo
);

alter table kontakt
    owner to postgres;

create table kontakt_adresa
(
    kontakt_id bigint not null
        constraint fk9boc0xdftr224vui4dltd9int
            references kontakt,
    adresa_id  bigint not null
        constraint fk9jhppikd84vl7ibpi04inhh79
            references adresa
);

alter table kontakt_adresa
    owner to postgres;
